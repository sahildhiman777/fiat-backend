const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Transaction = mongoose.model('Transaction');
const Userwallets = mongoose.model('Userwallets');
const Customer = mongoose.model('Customer');
const CryptoJS = require("crypto-js");
const config = require('../config/config');
const moment = require('moment');
const helper = require('./helper');
const Currency = mongoose.model('Currency');
const Settings = mongoose.model('Settings');
const io = require('socket.io')();
const QRCode = require('qrcode');
const request = require("request");
const rateLimit = require("express-rate-limit");
const apiLimiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 5,
    message: "Too many requests from this IP, please try again after sometime",
    skipFailedRequests: true
});
const PayusAPI = require('@payus/payus-sdk');
const mail = require('./mail');
//const Settings = mongoose.model('Settings');

const notify_exchange = (transaction) => {
    const notify_url = transaction.notify_url;

    // delete transaction['_id'];
    delete transaction['deposit'];
    delete transaction['edited'];
    delete transaction['updated_at'];
    delete transaction['notify_url'];
    delete transaction['return_url'];
    delete transaction['edited_by'];
    delete transaction['edited_at'];
    delete transaction['notifyHold'];
    delete transaction['__v'];

    console.log('Notify transaction', transaction);
    const str = JSON.stringify(transaction);
    const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
    var options = {
        method: 'POST',
        url: notify_url,
        headers: { 'content-type': 'application/json' },
        body: {
            data: ciphertext
        },
        json: true
    };
    request(options, function (error, response, body) {
        console.log(options, body);
    });
}

const socketUpdate = () => {
    var options = {
        method: 'POST',
        url: config.socket_update,
        headers: { 'content-type': 'application/json' },
        body: {
            data: []
        },
        json: true
    };
    request(options, function (error, response, body) {
        console.log(options, body);
    });
}

router.post('/', async (req, res, next) => {
    const { body: {
        order, bank
    }
    } = req;

    const bytes = CryptoJS.AES.decrypt(order.user, config.secretAes);
    const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    const object = JSON.parse(plaintext);

    if (!object) {
        const data = {
            status: false,
            data: 'Order Not found',
        };
        const str = JSON.stringify(data);
        const ciphertext = CryptoJS.AES.encrypt(str, config.secretAes);
        res.json({ status: false, data: ciphertext });
        return;
    }

    const currency = await Currency.find({ currency_code: object.currency_code }).select('_id').then(d => d).catch(e => { return false })
    let CurrId = '';
    if (currency.length > 0) {
        CurrId = currency[0]._id;
    }

    const neworder = object;
    neworder.bank = bank;
    neworder.created_at = new Date();
    neworder.updated_at = new Date();
    neworder.status = 'pending';
    neworder.payus_status = 'pending';
    neworder.currency = CurrId;
    neworder.deposit = true;
    neworder.edited = false;

    return Transaction.init().then(async () => {
        const curr = new Transaction(neworder);
        await curr.populate('bank', '_id bank_name').execPopulate();
        await curr.populate('currency', '_id currency currency_code').execPopulate();
        return curr.save();
    }).then(async (tr) => {
        const customerObj = {};
        customerObj.email = tr.email;
        customerObj.phone = tr.phone;
        const IsExist = await Customer.find(customerObj).then((data) => (data));
        if (IsExist.length === 0) {
            customerObj.status = true;
            customerObj.created_at = new Date();
            customerObj.updated_at = new Date();
            const customer = new Customer(customerObj);
            await customer.save();
        }
        let notifyObj = Object.assign({}, tr.toObject());
        socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
        const str = JSON.stringify(tr);
        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
        notify_exchange(notifyObj);
        res.json({
            status: true,
            data: ciphertext,
        })
    }).catch((err) => {
        const strErr = JSON.stringify(err);
        const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
        res.json({
            status: false,
            data: ciphertextErr,
        })
    });
});
router.get('/', async (req, res, next) => {

    //await function (deposit=false, dashboard =true,time='month')

    const dailyWithdraws = await helper.groupedSum(false, true, 'day');
    const monthlyWithdraws = await helper.groupedSum(false, true, 'month');
    const weeklyWithdraws = await helper.groupedSum(false, true, 'week');
    const hourlyWithdraws = await helper.groupedSum(false, true, 'hour');
    const totalWithdraws = await helper.aggrecate(false, 'all', 'total');
    const dailyDeposit = await helper.groupedSum(true, true, 'day');
    const monthlyDeposit = await helper.groupedSum(true, true, 'month');
    const weeklyDeposit = await helper.groupedSum(true, true, 'week');
    const hourlyDeposit = await helper.groupedSum(true, true, 'hour');
    const totalDeposit = await helper.aggrecate(true, 'all', 'total');

    const deposits = await helper.transactionsSearch(true, start, end, cur, input);
    const withdraws = await helper.transactionsSearch(false, start, end, cur, input);
    return res.status(200).json({
        status: true,
        data: {
            deposits, withdraws,
            dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});
router.get('/qrcode', async (req, res, next) => {
    const d = req.query.code;
    QRCode.toDataURL(d)
        .then(url => {
            var base64Data = url.replace(/^data:image\/png;base64,/, '');
            var img = new Buffer(base64Data, 'base64');

            res.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            res.end(img);
            // console.log(url)
        })
        .catch(err => {
            // res.json({ err });
            console.error(err)
        })


});
router.get('/search', async (req, res, next) => {
    const { body: {
        start, end, currency, input, status, bank, type
    }
    } = req;

    // const deposits = await helper.transactionsSearch(true, start, end, currency, input, status, bank);
    // const withdraws = await helper.transactionsSearch(false, start, end, currency, input, status, bank);
    const searchData = await helper.transactionsSearch('sell', start, end, 'VND', input, status, bank);

    return res.status(200).json({
        status: true,
        data: {
            searchData: searchData.tr, searchDataGrouped: searchData.grouped
            // deposits: deposits.tr, withdraws: withdraws.tr, depositsgrouped: deposits.grouped, withdrawsgrouped: withdraws.grouped,
            // dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            // dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});
router.post('/search', auth.required, auth.isAdmin, async (req, res, next) => {
    const { body: {
        start, end, currency, input, status, bank, type, time, timezone, loggedInUserData
    }
    } = req;

    const searchData = await helper.transactionsSearch(type, start, end, currency, input, status, bank, time, timezone, loggedInUserData);

    return res.status(200).json({
        status: true,
        data: {
            searchData: searchData.tr, searchDataGrouped: searchData.grouped
        },
    });
});

router.post('/list', async (req, res, next) => {
    const {
        type,
        status,
        platform,
        currency_code,
        searchText,
        pageNumber
    } = req.body

    const data = await helper.transactions(type, status, platform, currency_code, searchText, pageNumber);
    return res.status(200).json({
        status: true,
        data: data
    });

});
router.post('/dashboard', async (req, res, next) => {
    const {
        currency_code
    } = req.body
    //await function (deposit=false, dashboard =true,time='month')
    const dailyWithdraws = await helper.groupedSum(false, true, 'day', currency_code);
    const monthlyWithdraws = await helper.groupedSum(false, true, 'month', currency_code);
    const weeklyWithdraws = await helper.groupedSum(false, true, 'week', currency_code);
    const hourlyWithdraws = await helper.groupedSum(false, true, 'hour', currency_code);
    const totalWithdraws = await helper.aggrecate(false, 'all', 'total', currency_code);
    const dailyDeposit = await helper.groupedSum(true, true, 'day', currency_code);
    const monthlyDeposit = await helper.groupedSum(true, true, 'month', currency_code);
    const weeklyDeposit = await helper.groupedSum(true, true, 'week', currency_code);
    const hourlyDeposit = await helper.groupedSum(true, true, 'hour', currency_code);
    const totalDeposit = await helper.aggrecate(true, 'all', 'total', currency_code);
    return res.status(200).json({
        status: true,
        data: {
            dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});

router.get('/single/:cId', auth.required, auth.isAdmin, async (req, res, next) => {
    const { payload: { id } } = req;
    const cId = req.params.cId;

    Transaction.findById(cId).populate('bank').then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});
router.post('/withdraws/search', auth.required, auth.isAdmin, async (req, res, next) => {
    const { body: {
        search
    }
    } = req;
    if (!search) {
        return res.status(200).json({
            status: false,
            data: 'search parameter required',
        });
    } else if (!search.name) {
        return res.status(200).json({
            status: false,
            data: 'search parameter column required',
        });
    } else if (search.name == 'date') {
        const tonight = moment('search').endOf('day').toString();
        // db.users.find({ "name": /m/ })

        Transaction.find({
            created_at: {
                $gte: new Date(start),
                $lte: new Date(tonight)
            },
            deposit: false,
        }).populate('bank').then(d => {
            return res.status(200).json({
                status: true,
                data: d,
            });
        }).catch(err => {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        });
    } else {
        //db.users.find({"name": /m/})
        Transaction.find(search).populate('bank').then(d => {
            return res.status(200).json({
                status: true,
                data: d,
            });
        }).catch(err => {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        });
    }

});
router.get('/report', async (req, res, next) => {
    //await helper.aggrecate(deposit or withdraws, report time, either total money or coin_amount,)
    // const dailyWithdraws = await helper.aggrecate(false, 'daily', 'total');
    // const weeklyWithdraws = await helper.aggrecate(false, 'week', 'total');
    // const monthlyWithdraws = await helper.aggrecate(false, 'month', 'total');
    // const totalWithdrws = await helper.aggrecate(false, 'all', 'total');
    // const dailyDeposit = await helper.aggrecate(true, 'daily', 'total');
    // const weeklyDeposit = await helper.aggrecate(true, 'week', 'total');
    // const monthlyDeposit = await helper.aggrecate(true, 'month', 'total');
    // const totalDeposit = await helper.aggrecate(true, 'all', 'total');
    const deposits = await helper.transactions(true, false /*'pending'*/);
    const withdraws = await helper.transactions(false, false /*'pending'*/);
    return res.status(200).json({
        status: true,
        data: {
            deposits, withdraws, dailyWithdraws, weeklyWithdraws, monthlyWithdraws, totalWithdrws,
            dailyDeposit, weeklyDeposit, monthlyDeposit, totalDeposit
        },
    });
});

router.put('/:cId', auth.required, auth.isAdmin, async (req, res, next) => {
    const { body: {
        bank,
        status,
        loggedInUserData
    }
    } = req;

    const cId = req.params.cId;
    const tr = await Transaction.findById(cId).then(d => d);
    if (tr.edited) {
        return res.status(200).json({
            status: false,
            data: 'Cannot edit this transaction.',
        });
    } else {

        const c = {};
        c.status = status == 'approved' ? 'approved' : 'rejected';
        c.edited = true;
        c.updated_at = new Date();
        c.edited_by = loggedInUserData._id;
        c.edited_at = new Date();

        if (tr.deposit === false)
            c.bank = bank;

        const cId = req.params.cId;
        // const notifyData = {
        //     id: req.params.cId,
        //     status,
        // }

        if (c.status === 'approved' && tr.deposit === true) {
            const payus = new PayusAPI(config.payusAccessToken);

            var payusResult = payus.withdraw({
                coin_code: tr.coin_name,
                amount: tr.coin_amount,
                destination_tag: tr.payus_destination_tag,
                withdraw_address: tr.coin_address,
                order_id: tr.transaction_id
            });

            return payusResult.then(result => {
                if (result.status === 'success') {
                    console.log('payus withdraw result', result);

                    c.payus_transaction_id = result.data.transaction_id;
                    c.transaction_hash = result.data.tx_hash
                    c.payus_status = c.transaction_hash ? 'completed' : 'waiting_for_confirmation';
                    const updateObj = c;
                    Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                        if (model) {
                            // helper.notify(notifyData);
                            Transaction.findById(cId).populate('bank').populate('currency').then(d => {
                                Userwallets.findOneAndUpdate(
                                    {
                                        user_id: d.user_id,
                                        coin_code: d.coin_name
                                    }, {
                                        $inc: { coin_balance: Number(d.coin_amount) }
                                    }
                                )
                                    .then(() => console.log("Wallet updated successfully"))
                                    .catch(err => console.log("Error updating wallet", err));

                                let notifyObj = Object.assign({}, d.toObject());
                                socketUpdate();
                                mail.requestNotification({
                                    email: notifyObj.email,
                                    type: notifyObj.deposit ? 'Buy' : 'Sell',
                                    amount: notifyObj.coin_amount,
                                    coin_code: notifyObj.coin_name,
                                    status: notifyObj.status,
                                    link: config.BaseUrl + "/transactions/"
                                });

                                if (notifyObj.platform != "exchangefiat") {
                                    notify_exchange(notifyObj);
                                }
                                return res.status(200).json({
                                    status: true,
                                    data: d,
                                });
                            }).catch(err => {
                                return res.status(200).json({
                                    status: false,
                                    data: err.toString(),
                                });
                            });
                        }
                        else if (err) {
                            return res.status(200).json({
                                status: false,
                                data: err.toString(),
                            });
                        }

                    });
                } else {
                    console.log('payus withdraw error', result)
                    return res.json({
                        status: false,
                        data: result.message,
                    })
                }

            }).catch(error => {
                console.log('payus withdraw error', error)
                return res.json({
                    status: false,
                    data: error.toString(),
                })
            })
        } else {
            const updateObj = c;
            Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                if (model) {
                    // helper.notify(notifyData);
                    Transaction.findById(cId).populate('bank').populate('currency').then(d => {
                        if (d.status === 'approved' && d.platform === "exchangefiat" && d.deposit === false) {
                            const payus = new PayusAPI(config.payusAccessToken);

                            var payusResult = payus.withdraw({
                                coin_code: d.coin_name,
                                amount: d.coin_amount,
                                destination_tag: d.payus_destination_tag,
                                withdraw_address: d.coin_address,
                                order_id: d.transaction_id
                            });

                            payusResult.then(result => {
                                if (result.status === 'success') {
                                    d.payus_transaction_id = result.data.transaction_id;
                                    d.transaction_hash = result.data.tx_hash
                                    d.payus_status = d.transaction_hash ? 'completed' : 'waiting_for_confirmation';
                                    return d.save();
                                }
                            })
                        } else if (d.status === 'rejected' && d.platform === "exchangefiat" && d.deposit === false) {
                            Userwallets.findOneAndUpdate(
                                {
                                    user_id: d.user_id,
                                    coin_code: d.coin_name
                                },
                                {
                                    $inc: { coin_balance: Number(d.coin_amount) }
                                })
                                .then(() => console.log("Wallet updated successfully"))
                                .catch(err => console.log("Error updating wallet", err));
                        }
                        let notifyObj = Object.assign({}, d.toObject());
                        socketUpdate();
                        mail.requestNotification({
                            email: notifyObj.email,
                            type: notifyObj.deposit ? 'Buy' : 'Sell',
                            amount: notifyObj.coin_amount,
                            coin_code: notifyObj.coin_name,
                            status: notifyObj.status,
                            link: config.BaseUrl + "/transactions/"
                        });
                        if (notifyObj.platform != "exchangefiat") {
                            notify_exchange(notifyObj);
                        }
                        return res.status(200).json({
                            status: true,
                            data: d,
                        });
                    }).catch(err => {
                        return res.status(200).json({
                            status: false,
                            data: err.toString(),
                        });
                    });
                }
                else if (err) {
                    return res.status(200).json({
                        status: false,
                        data: err.toString(),
                    });
                }

            });
        }
    }
});

router.delete('/:cId', auth.required, auth.isAdmin, async (req, res, next) => {

    const cId = req.params.cId;
    Transaction.findByIdAndRemove(cId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});
router.get('/status/:cId', async (req, res, next) => {
    const cId = req.params.cId;

    Transaction.findById(cId).then(d => {
        return res.status(200).json({
            status: true,
            data: d.status
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});
router.post('/withdraw', apiLimiter, async (req, res, next) => {
    let order = req.body.order;
    let validationError = false;

    const bytes = CryptoJS.AES.decrypt(order, config.secretAes);
    const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    if (!plaintext) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    object = JSON.parse(plaintext);
    // const object = (order);
    console.log('sell order create', object)
    if (!object) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    const fields = ["total", "user_id", "platform", "coin_amount", "coin_name", "email", "phone", "user_bank_name", "user_bank_account_number", "user_bank_acount_holder_name", "user_currency_code"];
    fields.forEach((field) => {
        if (!object[`${field}`]) {
            validationError = true;
            res.json({ status: false, data: `${field} is required` });
            return;
        }
    });

    if (validationError) {
        return;
    }

    const {
        coin_name,
        coin_amount,
        email,
        notify_url,
        return_url,
        transaction_id
    } = object;

    const neworder = object;
    neworder.created_at = new Date();
    neworder.updated_at = new Date();
    neworder.status = 'pending';
    neworder.edited = false;
    neworder.deposit = false;

    const payus = new PayusAPI(config.payusAccessToken);

    var payusResult = payus.paymentCryptoRest({
        currency: coin_name,
        return_url: config.notify_me,
        notify_url: config.notify_me,
        coin_code: coin_name,
        product_amount: coin_amount,
        invoice_id: transaction_id,
        email: email
    });

    return payusResult.then(result => {
        console.log('payus deposit result', result)
        if (result.status === 'success') {
            neworder.payus_transaction_id = result.data.result.transaction_id;
            neworder.payus_amount_orignal = result.data.result.amount;
            neworder.payus_amount_usd = result.data.product_amount;
            neworder.payus_amount_crypto = result.data.result.product_crypto_amount;
            neworder.payus_destination_tag = result.data.result.destination_tag;
            neworder.payus_mining_fees = result.data.result.mining_fees;
            neworder.payus_total_payable_amount = result.data.result.total_payable_amount;
            neworder.payus_fees = result.data.result.payus_fees;
            neworder.payus_total_fees = result.data.result.fees;
            neworder.payus_usd_rate = result.data.result.usd_rate;
            neworder.payus_expire_datetime = result.data.result.expire_datetime;
            neworder.payus_payment_tracking = result.data.result.payment_tracking;
            neworder.coin_address = result.data.result.crypto_address;
            neworder.payus_status = 'pending';
        } else {
            console.log('payus deposit error on call', result)
            const strErr = JSON.stringify(result);
            const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: ciphertextErr,
            })
        }
        return Transaction.init().then(async () => {
            const curr = new Transaction(neworder);
            await curr.populate('bank', '_id bank_name').execPopulate();
            await curr.populate('currency', '_id currency currency_code').execPopulate();
            return curr.save();
        }).then(async (tr) => {
            const customerObj = {};
            customerObj.email = tr.email;
            customerObj.phone = tr.phone;
            const IsExist = await Customer.find(customerObj).then((data) => (data));
            if (IsExist.length === 0) {
                customerObj.status = true;
                customerObj.created_at = new Date();
                customerObj.updated_at = new Date();
                const customer = new Customer(customerObj);
                await customer.save();
            }
            socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
            let notifyObj = Object.assign({}, tr.toObject());
            const str = JSON.stringify(tr);
            const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
            notify_exchange(notifyObj);
            return res.json({
                status: true,
                data: ciphertext,
            });
        }).catch((err) => {
            const strErr = JSON.stringify(err);
            const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: ciphertextErr,
            })
        });
    }).catch(error => {
        console.log('payus deposit error', error)
        const strErr = JSON.stringify(error);
        const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
        return res.json({
            status: false,
            data: ciphertextErr,
        })
    })
});

router.post('/withdraw/:cId', apiLimiter, async (req, res, next) => {
    const cId = req.params.cId;
    let validationError = false;

    let order = req.body.order;

    const bytes = CryptoJS.AES.decrypt(order, config.secretAes);
    const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    if (!plaintext) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    object = JSON.parse(plaintext);
    // const object = (order);
    console.log('call after approval', object)
    if (!object) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    const fields = ["transaction_hash", "coin_address"];
    fields.forEach((field) => {
        if (!object[`${field}`]) {
            validationError = true;
            res.json({ status: false, data: `${field} is required` });
            return;
        }
    });

    if (validationError) {
        return;
    }

    Transaction.findById(cId).then(tr => {

        const payus = new PayusAPI(config.payusAccessToken);

        var payusResult = payus.getBalance({
            transaction_id: tr.payus_transaction_id
        });

        return payusResult.then(result => {
            console.log('payus address balance result', result)
            const c = {};
            c.updated_at = new Date();
            c.transaction_hash = object['transaction_hash'];
            c.coin_address = object['coin_address'];
            c.payus_status = tr.payus_status == 'pending' ? 'waiting_for_confirmation' : tr.payus_status;
            c.actual_deposit_amount = typeof result.data == 'object' ? 0 : parseFloat(result.data).toFixed(8);

            const updateObj = c;

            Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                if (model) {
                    Transaction.findById(cId).populate('bank').populate('currency').then(d => {
                        socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
                        let notifyObj = Object.assign({}, d.toObject());
                        const str = JSON.stringify(d);
                        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
                        notify_exchange(notifyObj);
                        return res.json({
                            status: true,
                            data: ciphertext,
                        });
                    }).catch(err => {
                        return res.status(200).json({
                            status: false,
                            data: err.toString(),
                        });
                    });
                }
                else if (err) {
                    return res.status(200).json({
                        status: false,
                        data: err.toString(),
                    });
                }
            });
        }).catch(error => {
            console.log('payus address balance error', error)
            const strErr = JSON.stringify(error);
            const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: ciphertextErr,
            })
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.post('/notify_me', async (req, res, next) => {
    console.log('Transaction notify from payus', req.body);
    const {
        body: {
            invoice_id,
            transaction_id,
            txid,
            coin_code,
            product_amount_crypto,
            product_amount_base,
            crypto_address,
            org_amount,
            payus_fees,
            mining_fees,
            status
        }
    } = req;

    Transaction.findOne({
        payus_transaction_id: transaction_id,
    }).then(tr => {
        let notifyObj = Object.assign({}, tr.toObject());

        notifyObj.payus_status = status;

        // if(tr.actual_deposit_amount){
        //     notify_exchange(notifyObj);
        //     return res.status(200).json({
        //         status: true,
        //         txid: tr.transaction_hash,
        //     });
        // }
        const c = {};
        c.updated_at = new Date();
        c.payus_status = status;
        if (status == 'completed' || status == 'waiting_for_confirmation') {
            c.actual_deposit_amount = parseFloat(product_amount_crypto).toFixed(8);
            notifyObj.actual_deposit_amount = c.actual_deposit_amount;
        }

        if (status === 'expired' && tr.status === 'pending')
            c.status = 'rejected';

        if (!tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation'))
            c.notifyHold = true;

        if (tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation'))
            c.notifyHold = false;

        const updateObj = c;

        Transaction.findByIdAndUpdate(tr._id, updateObj, { new: true }, function (err, model) {
            if (model) {
                socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
                if (status !== 'pending')
                    notifyObj.status = status;

                if (status === 'expired' && tr.status === 'pending')
                    notifyObj.status = 'rejected';

                if (status === 'pending' || (tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation')) || (status === 'expired' && tr.status === 'pending'))
                    notify_exchange(notifyObj);

                return res.status(200).json({
                    status: true,
                    txid: txid,
                });
            }
            else if (err) {
                return res.status(200).json({
                    status: false,
                    data: err.toString(),
                });
            }
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

module.exports = router;