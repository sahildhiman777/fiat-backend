const router = require('express').Router();
const mongoose = require('mongoose');
var speakeasy = require('speakeasy');
const Otp = mongoose.model('Otp');
const Users = mongoose.model('Users');
const auth = require('./auth');
var QRCode = require('qrcode');

// to create 2fa (QR code)
router.post('/:id',  async (req, res, next) => {
  const userId = req.params.id;  console.log('Usr_ID', userId);
  try
  {
    Users.findOne({ _id: userId })
    .then( user => {
      if(!user.google2fa && (!user.faKey || !user.otpauth_url)){
        let faKey = null;
        
        // Generate a secret, if needed
        var secret = speakeasy.generateSecret({length: 20});
        // By default, generateSecret() returns an otpauth_url for SHA1
        
        // Use otpauthURL() to get a custom authentication URL for SHA512
        var url = speakeasy.otpauthURL({ secret: secret.ascii, label: 'ExchangeFiat', algorithm: 'sha512' });
        
        const newData = {}
        newData.faKey = secret.base32;
        newData.otpauth_url = url;

        Users.findByIdAndUpdate({ _id: userId }, { $set: newData }, {new: true}).then(userUpdate =>{
          console.log('userUpdate', userUpdate);
        })
        faKey = secret.base32

        // Pass URL into a QR code generator
        QRCode.toDataURL(url, function(err, image_data) {
          return res.json({status: true, data: faKey, image_data, user});
        });
      }
      else{
        // Pass URL into a QR code generator
        QRCode.toDataURL(user.otpauth_url, function(err, image_data) {
          return res.json({status: true, data: user.faKey, image_data, user});
        });
      }
    });
     
  }
  catch(err) {
    return res.json({status: false, data: err});
  }
 });

//enable QR code (2FA)
router.put('/enable/:id',  async(req, res, next) => {
  const {  google2fa,  code } = req.body;
  const userId = req.params.id;
  const enableUser2fa = {}
  enableUser2fa.google2fa = google2fa;
  Users.findOne({ _id: userId })
  .then( user => {
    console.log('user in enable api==7**7=>', user);
    var verified = speakeasy.totp.verify({ secret: user.faKey,
      encoding: 'base32',
      token: code,
      window: 10,  
   });
   if (!verified) {
    return res.status(200).json({
      status: false,
      data: 'Secret code is wrong.',
    });
  }
  if(verified) {
    Users.findByIdAndUpdate({ _id: userId }, { $set: enableUser2fa }, {new: true}).then(enabled => {
      // console.log('enabled 2fa', enabled);
     try{
       return res.json({
         status: 200,
         message: "2fa enabled successfully",
         data: enabled
       })
   }catch(err)  {
       return res.json({
         status: "error",
         message: err,
     })
      }
   });
   }
  });
})

//disable QR code (2FA)
router.put('/disable/:id',  async(req, res, next) => {
  const {   google2fa, code } = req.body;  
  const userId = req.params.id;
  const disableUser2fa = {}
   disableUser2fa.google2fa = google2fa;
  Users.findOne({ _id: userId })
  .then( user => {
    var verified = speakeasy.totp.verify({ secret: user.faKey,
      encoding: 'base32',
      token: code,
      window: 10,
  });
  if (!verified) {
    return res.status(200).json({
      status: false,
      data: 'Secret code is wrong',
    });
  }
  if(verified) {
    Users.findByIdAndUpdate({ _id: userId }, { $set: disableUser2fa }, {new: true}).then(disabled => {
      // console.log('disabled 2fa', disabled);
  
      try{
        return res.json({
          status: 200,
          message: "2fa disabled successfully",
          data: disabled
        })
    }catch(err)  {
        return res.json({
          status: "error",
          message: err,
      })
       }
       
    })
   }
});
})


router.post('/', auth.required, async  (req, res, next) => {
  console.log('end point hit of google otp')
  const { body: { userToken } } = req;
  if(!userToken){
    res.json({
        status: false,
        data :'A valid OTP required',
    }); 
  }
  const   authorization   = req.headers.authorization;
  let token ='';
  if(authorization && authorization.split(' ')[0] === 'Bearer') {
    token = authorization.split(' ')[1];
  }

  const { payload: { id } } = req;
  if(!id) {
    return res.status(200).json({
        status:false,
        data: 'Please Login',
        debug: 'token not found'
    });
}
  const user = await Users.findById(id).then((data) =>data);
 
    if(!user) {
    return res.status(200).json({
        status:false,
        data: 'Please Login',
    });
    }
  
    console.log('user.fakey ===>>', user.faKey);


      //  const  secret = 'KQUV2SZWI5FHCZKDOZ2UYUBOLA2TAODC';
      
  // Verify that the user token matches what it should at this moment
  const verified = speakeasy.totp.verify({
    //secret: user.faKey ? user.faKey: secret,
    secret:  user.faKey,
    encoding: 'base32',
    token: userToken,
    window: 10,
  });
  if(!verified){
    res.json({
        status: false,
        data :'Otp is not valid',
        });
  } else {
    const toSave ={
        token,status:true 
    }
    console.log('toSave',toSave)
    otpVerified = new Otp(toSave); 
    const otpVerifySaved = await otpVerified.save().then(d=>d);
    console.log('otpVerifySaved',otpVerifySaved)
    if(otpVerifySaved){
      return res.json({
        status: true,
        data :verified,
        });
    } else {
      return res.json({
        status: false,
        data :'Oops!! something went wrong ',
       
        });
    }
  }
});
module.exports = router;