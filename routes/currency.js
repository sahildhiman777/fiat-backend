const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Currency = mongoose.model('Currency');

router.post('/', auth.required, auth.isAdmin, async (req, res, next) => {
    const { body: {
        country,
        currency,
        currency_code,
        file
    }
    } = req;

    if ( !country || !currency || !currency_code) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const c = {};
    c.country = country.toUpperCase();
    c.currency = currency;
    c.currency_code = currency_code.toUpperCase();
    c.logo = file
    
    const IsExist = await Currency.find(c).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Currency already exist ',
        });
    }
    const curr = new Currency(c);
    return curr.save()
        .then(() => {
            res.json({
                status: true,
                data:  c ,
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
});




router.get('/', auth.required, auth.isAdmin, async (req, res, next) => {
    const { payload: { id } } = req;
    Currency.find().then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.get('/:cId', auth.required, auth.isAdmin, async (req, res, next) => {
    const { payload: { id } } = req;
    const cId = req.params.cId;

    Currency.findById(cId).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.put('/:cId', auth.required, auth.isAdmin, async (req, res, next) => {
    const { body: {
        country,
        currency,
        currency_code,
        file
    }
    } = req;

    if ( !country || !currency || !currency_code) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const cId = req.params.cId;
    const c = {};
    c.country = country.toUpperCase();
    // c.currency = currency;
    c.logo = file;
    c.currency_code = currency_code.toUpperCase();
    
    const IsExist = await Currency.find({c,  _id: { $ne: cId },}).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Currency exist with same country and Currency Code',
        });
    }
    c.currency = currency;
    const updateObj = c;
    
    Currency.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

router.delete('/:cId', auth.required, auth.isAdmin, async (req, res, next) => {

    const cId = req.params.cId;
    Currency.findByIdAndRemove(cId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});


module.exports = router;