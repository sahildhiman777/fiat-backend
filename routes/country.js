const mongoose = require('mongoose');
const router = require('express').Router();
const Currency = mongoose.model('Currency');
const config = require('../config/config');
const CryptoJS = require("crypto-js");

router.get('/list', async (req, res) => {
    const currency = await Currency.aggregate([
        { $group: { _id: "$country", currency: {$push: "$currency_code"} } }]).then(c => c);
    
    let countries = currency.map(function(c){
        let d = [];
        d.push(c.currency[0]);
        d.push(c._id);
        return d;
    });
    const str = JSON.stringify(countries);

    const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
    return res.status(200).json({
        status: true,
        data: ciphertext
    });
});


module.exports = router;