const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('./auth');
const Users = mongoose.model('Users');
var speakeasy = require('speakeasy');
var validator = require("email-validator");
const crypto = require('crypto');
const userKyc = mongoose.model('userKyc');
//const mail =require("./mailer");
const mail = require('./mail');
//POST new user route (optional, everyone has access)

router.post('/', auth.required, auth.isSuperman, async (req, res, next) => {
  // mail.adminWelcomeEmail('d');
  // return res.status(200).json({
  //   status: false,
  //   data: 'Email is required',
  // });
  const { body: { email, password, name, role } } = req;

  if (!email) {
    return res.status(200).json({
      status: false,
      data: 'Email is required',
    });
  }

  if (!password) {
    return res.status(200).json({
      status: false,
      data: 'Password is required',
    });
  }
  if (!role) {
    return res.status(200).json({
      status: false,
      data: 'Select a Role',
    });
  }
  if (!name) {
    return res.status(200).json({
      status: false,
      data: 'Provide  Name',
    });
  }
  const user = {};
  user.email = email;
  user.password = password;
  user.role = role;
  user.name = name;
  const secret = speakeasy.generateSecret({ length: 20 });
  user.faKey = secret.base32;
  user.otpauth_url = secret.otpauth_url;
  const finalUser = new Users(user);
  const IsExist = await Users.find({ email: user.email }).then((data) => (data));
  if (IsExist.length > 0) {
    return res.status(200).json({
      status: false,
      data: 'Email already taken ',
    });
  }
  if (!validator.validate(user.email)) {
    return res.status(200).json({
      status: false,
      data: 'Email not valid',
      IsExist: IsExist,
    });
  }
  finalUser.setPassword(user.password);
  const newAdmin = await finalUser.save()
    .then((d) => d)
    .catch((err) => res.json({
      status: false,
      data: err.toString(),
    }));
  console.log('newAdmin', newAdmin)
  if (newAdmin) {
    const sendmail = await mail.adminWelcomeEmail(newAdmin);
    //send email to user
    return res.json({
      status: true,
      data: newAdmin,
    })
  }

});

//POST login route (optional, everyone has access)
router.post('/login', auth.adminLogin, (req, res, next) => {
  const { body: { email, password } } = req;

  const user = {};
  user.email = email;
  user.password = password;
  if (!user.email) {
    return res.status(200).json({
      status: false,
      data: 'Email is required',
      debug: req.body
    });
  }

  if (!user.password) {
    return res.status(200).json({
      status: false,
      data: 'Password is required',
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    if (err) {
      return res.status(200).json({
        status: false,
        data: 'Credentials Invalid',
      });
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();
      return res.json({ status: true, data: { user: user.toAuthJSON() } });

    }
    return res.status(200).json({
      status: false,
      data: 'Credentials Invalid',
    });
    return status(400).info;
  })(req, res, next);
});

//Change user name and password
router.put('/changepassword', auth.required, async (req, res, next) => {
  const { name, password } = req.body;
  const newPass = password
  const userId = req.body.id;

  Users.findOne({ _id: userId })
    .then(user => {
      // console.log('user is finded', user);
    });

  const newUser = {}

  if (!newPass) {
    newUser.name = name;
  }
  else {
    const salt = this.salt = crypto.randomBytes(16).toString('hex');
    const hash = this.hash = crypto.pbkdf2Sync(newPass, this.salt, 10000, 512, 'sha512').toString('hex');

    newUser.name = name;
    newUser.salt = salt;
    newUser.hash = hash;
  }



  Users.findByIdAndUpdate({ _id: userId }, { $set: newUser }).then(changed => {
    try {
      return res.json({
        status: 200,
        message: "Password changed successfully",
        data: changed
      })
    } catch (err) {
      return res.json({
        status: "error",
        message: err,
      })
    }
  });
})

//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, (req, res, next) => {
  const { payload: { id } } = req;

  return Users.findById(id)
    .then((user) => {
      if (!user) {
        return res.status(200).json({
          status: false,
          data: 'Please Login',
        });
      }
      return res.status(200).json({
        status: true,
        data: { user: user.toAuthJSON() }
      });
      // return res.json({ user: user.toAuthJSON() });
    });
});


router.get('/', auth.required, auth.isSuperman, async (req, res, next) => {
  const { payload: { id } } = req;
  Users.find({ _id: { $ne: id } }).select(['id', 'name', 'role', 'email', 'faKey']).then(d => {
    return res.status(200).json({
      status: true,
      data: d,
    });
  }).catch(err => {
    return res.status(200).json({
      status: false,
      data: err.toString(),
    });
  });
});

router.get('/:adminid', auth.required, auth.isSuperman, async (req, res, next) => {
  const { payload: { id } } = req;
  const adminId = req.params.adminid;

  Users.findById(adminId).select(['id', 'name', 'role', 'email', 'faKey']).then(d => {
    return res.status(200).json({
      status: true,
      data: d,
    });
  }).catch(err => {
    return res.status(200).json({
      status: false,
      data: err.toString(),
    });
  });
});

router.put('/:adminid', auth.required, auth.isSuperman, async (req, res, next) => {
  const { body: { role, name, email, kyc_level1, kyc_level2, kyc_level3 } } = req;
  const adminId = req.params.adminid;
  const IsExist = await Users.find({ email: email, _id: { $ne: adminId } }).then((data) => (data));
  if (IsExist.length > 0) {
    return res.status(200).json({
      status: false,
      data: 'Email already taken ',
    });
  }
  if (!validator.validate(email)) {
    return res.status(200).json({
      status: false,
      data: 'Email not valid',
      IsExist: IsExist,
    });
  }
  const updateObj = {};
  if (!!role) {
    updateObj.role = role;
  }
  if (!!name) {
    updateObj.name = name;
  }
  if (!!email) {
    updateObj.email = email;
  }
  if (!!kyc_level1) {
    updateObj.kyc_level1 = kyc_level1;
  }
  if (!!kyc_level2) {
    updateObj.kyc_level2 = kyc_level2;
  }
  if (!!kyc_level3) {
    updateObj.kyc_level3 = kyc_level3;
  }
  if (!(Object.keys(updateObj).length > 0)) {
    return res.status(200).json({
      status: false,
      data: "Something went wrong",
    });
  }
  Users.findByIdAndUpdate(adminId, updateObj, { new: true }, function (err, model) {
    if (model) {
      model.salt = false;
      model.hash = false;
      return res.status(200).json({
        status: true,
        data: model,
      });
    }
    else if (err) {
      return res.status(200).json({
        status: false,
        data: err.toString(),
      });
    }

  });
});

router.delete('/:adminid', auth.required, auth.isSuperman, async (req, res, next) => {

  const adminId = req.params.adminid;
  Users.findByIdAndRemove(adminId, (error, data) => {
    if (error) {
      return res.status(200).json({
        status: false,
        data: error.toString(),
      });
    } else {
      return res.status(200).json({
        status: true,
        data: [],
      });

    }
  });
});

router.get('/2fa/:adminid', auth.required, auth.isSuperman, async (req, res, next) => {

  const adminId = req.params.adminid;
  // Generate a secret, if needed
  var secret = speakeasy.generateSecret({ length: 20 });
  // By default, generateSecret() returns an otpauth_url for SHA1
  // Use otpauthURL() to get a custom authentication URL for SHA512
  var url = speakeasy.otpauthURL({ secret: secret.ascii, label: 'ExchangeFiat', algorithm: 'sha512' });
  const updateObj = { faKey: secret.base32, otpauth_url: url, google2fa: false };

  Users.findByIdAndUpdate(adminId, updateObj, { new: true }, function (err, model) {
    if (model) {
      model.salt = false;
      model.hash = false;
      mail.otp(model);
      return res.status(200).json({
        status: true,
        data: model,
      });
    }
    else if (err) {
      return res.status(200).json({
        status: false,
        data: err.toString(),
      });
    }

  });
});
// get kyc users data
router.post('/getkycusers', auth.required, auth.isSuperman, async (req, res, next) => {
  let pageNum = req.body.pageNumber;
  let nPerPage = 25;
  let query = { "role": "user" }
  const d = await Users.find(query).sort({ updated_at: -1 }).skip(pageNum > 0 ? ((pageNum - 1) * nPerPage) : 0).limit(nPerPage).populate('user_id', '_id name role email').then(d => d).catch((err => { }));
  const t = await Users.find(query).then(d => d);
  const total = t.length
  
  let userKycRecord = await userKyc.find({});
  const obj = {
    data: d,
    kycData: userKycRecord,
    total: total
  }

  return res.status(200).json({
    status: true,
    data: obj
  });
});

//Change user name and password
router.put('/profile', auth.required, async (req, res, next) => {
  const userId = req.params.id;
  Users.findOne({ _id: userId })
    .then(user => {
    });
  const newUser = {}
  Users.findByIdAndUpdate({ _id: userId }, { $set: newUser }).then(update => {
    try {
      return res.json({
        status: 200,
        message: "Profile updated successfully",
        data: update
      })
    } catch (err) {
      return res.json({
        status: "error",
        message: err,
      })
    }
  });
})

module.exports = router;
