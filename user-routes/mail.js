'use strict';
const nodemailer = require('nodemailer');
const config = require('../config/config');
var fs = require('fs');
var ejs = require('ejs');
var readHTMLFile = function (path, callback) {
    fs.readFile(path, {
        encoding: 'utf-8'
    }, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        } else {
            callback(null, html);
        }
    });
};
module.exports = {
    adminWelcomeEmail: async function (data) {
        const senderEmail = 'support@giaodichcoin.com';
        const senderPassword = '@Kevin$007';
        const to = `${data.email}`;
        readHTMLFile(__dirname + '/../views/email.ejs', function (err, html) {
            console.log(__dirname);
            var template = ejs.compile(html);
            var replacements = {
                name: data.name,
                otp: data.faKey,
                imgsrc: `${config.App_url}${data.faKey}`,
            };
            var htmlToSend = template(replacements);
            let smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: senderEmail,
                    pass: senderPassword,
                },
            });
            var mailOptions = {
                from: senderEmail,
                to: to, subject: "Welcome at GDC",
                html: htmlToSend // html body
            };

            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                }
            });
        });
    },
    otp: async function (data) {
        const senderEmail = 'support@giaodichcoin.com';
        const senderPassword = '@Kevin$007';
        const to = `${data.email}`;
        readHTMLFile(__dirname + '/../views/otp.ejs', function (err, html) {
            console.log(__dirname);
            var template = ejs.compile(html);
            var replacements = {
                name: data.name,
                otp: data.faKey,
                imgsrc: `${config.App_url}${data.faKey}`,
            };
            var htmlToSend = template(replacements);
            let smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: senderEmail,
                    pass: senderPassword,
                },
            });
            var mailOptions = {
                from: senderEmail,
                to: to, subject: "Google 2FA key",
                html: htmlToSend // html body
            };

            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                }
            });
        });
    },

    forgotPassword: async function (data) {
        const senderEmail = 'processorpayment2017@gmail.com';
        const senderPassword = 'riegtfcvigcgyhgl';
        const to = `${data.email}`;
        readHTMLFile(__dirname + '/../views/forgotPassword.ejs', function (err, html) {
            var template = ejs.compile(html);
            var replacements = {
                name : data.name,
                email: data.email,
                link :   data.link,
            };
            var htmlToSend = template(replacements);
                
            let smtpTransport = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                auth: {
                    user: senderEmail,
                    pass: senderPassword
                }
            });
            var mailOptions = {
                from: senderEmail,
                to: to,
                subject: "ExchangeFiat- Forgot Password Request",
                html: htmlToSend // html body
            };
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                }
                console.log(response);
            });
        });
    },
    emailVerify: async function (data) {
        const senderEmail = 'processorpayment2017@gmail.com';
        const senderPassword = 'riegtfcvigcgyhgl';
        const to = `${data.email}`;
        readHTMLFile(__dirname + '/../views/emailverify.ejs', function (err, html) {
            var template = ejs.compile(html);
            var replacements = {
                name : data.name,
                email: data.email,
                link :   data.link,
            };
            var htmlToSend = template(replacements);
                
            let smtpTransport = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                auth: {
                    user: senderEmail,
                    pass: senderPassword
                }
            });
            var mailOptions = {
                from: senderEmail,
                to: to,
                subject: "Welcome at Exchange fiat",
                html: htmlToSend // html body
            };
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                }
                console.log(response);
            });
        });
    }

}