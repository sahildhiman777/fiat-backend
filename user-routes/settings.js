const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Settings = mongoose.model('Settings');
const Transaction = mongoose.model('Transaction');
const moment = require('moment');

router.put('/edit', auth.required, async (req, res, next) => {
    const { body: {
    totalWithdraws,
    status,
    loggedInUserData,
    currency_code
    }
    } = req;

    if (!totalWithdraws || !status) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const newSettings = {};
    newSettings.totalWithdraws = totalWithdraws;
    newSettings.status = status;
    newSettings.currency_code = currency_code;
    newSettings.user_id = loggedInUserData._id
    const IsExist = await Settings.find(newSettings).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'No changes Made',
        });
    }
    newSettings.created_at = new Date();
    newSettings.updated_at = new Date();
    const finalSetting = new Settings(newSettings);
    return finalSetting.save()
        .then((d) => {
            res.json({
                status: true,
                data:  d ,
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
});




router.put('/single', auth.required, async (req, res, next) => {
    console.log('req.body', req.body);
    
    const { body: { loggedInUserData, timezone, currency_code } } = req;
    let query = {};

    const starts = moment().startOf('day').toString();
    const ends = moment().endOf('day').toString();
    query.updated_at = {
        $gte: new Date(starts),
        $lte: new Date(ends)
    };

    query.user_id = loggedInUserData._id;
    query.currency_code = currency_code;
    
    console.log('query', query);
    
    const c = await Settings.find(query).sort({ updated_at: -1 }).then( async (d) => {
        let data = [], ps, tr, q = {};
        q.edited_at = query.updated_at;
        q.status = 'approved';
        q.deposit = false;
        q.edited_by = loggedInUserData._id;

        if(d.length){
            for (let i = 0; i < d.length ; i++) {
                ps = Object.assign({}, d[i].toObject());
                tr = await Transaction.aggregate([{ $match: q }, { $sort : { edited_at : -1 } }, 
                    {
                        $group: {
                            _id: {
                                "$dateToString": {"format": "%d-%m-%Y", "date": "$edited_at", "timezone": timezone}
                             },
                            total: { $sum: {"$toDouble": "$total"} },
                        }
                    }]);
                ps.total = tr.length ? tr[0].total : 0;
                data.push(ps);
            }
        }
        return data;
    });
        return res.status(200).json({
            status: true,
            data: c,
        });
   
});

router.post('/list', auth.required, async (req, res, next) => {   
    const { body: { loggedInUserData, pageNumber } } = req;
    let query = {}
    let pageNum = pageNumber;
    let nPerPage = 25;


    if(loggedInUserData && loggedInUserData.role !== 'superadmin') {
        query.user_id = loggedInUserData._id;
    }
     
    const d = await Settings.find(query).sort( { updated_at: -1 } ).skip( pageNum > 0 ? ( ( pageNum - 1 ) * nPerPage ) : 0 ).limit( nPerPage ).populate('user_id', '_id name role email').then(d => d).catch((err => { }));
    
    // const t = await Settings.find(query).sort( { updated_at: -1 } ).then(d => d).catch((err => { }));
    const t = await Settings.find(query).then(d =>d);
     const total = t.length
     const obj = {
        data:  d,
        total: total
     }

    return res.status(200).json({
        status: true,
        data: obj
    });
   
});

router.put('/edit/:Id', auth.required, async (req, res, next) => {
   const { body: {
        totalWithdraws,
        totalPaid,
        totalApproved,
        status,
        loggedInUserData,
        timezone,
        currency_code
    }
    } = req;


    if (!totalWithdraws || !totalPaid || !status) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const newSettings = {};
    newSettings.totalWithdraws = totalWithdraws;
    newSettings.totalPaid = totalPaid;
    newSettings.totalApproved = totalApproved;
    newSettings.user_id = loggedInUserData._id
    newSettings.status = status;
    newSettings.currency_code = currency_code;
    console.log('newSettings', newSettings)

    const starts = moment().startOf('day').toString();
    const ends = moment().endOf('day').toString();

    let tr, q = {};
        q.edited_at = {
            $gte: new Date(starts),
            $lte: new Date(ends)
        };
        q.status = 'approved';
        q.deposit = false;
        q.edited_by = loggedInUserData._id;

        tr = await Transaction.aggregate(
            [
                { $match: q },
                { $sort : { edited_at : -1 } }, 
                {
                    $group: {
                        _id: {
                            "$dateToString": {"format": "%d-%m-%Y", "date": "$edited_at", "timezone": timezone}
                            },
                        total: { $sum: {"$toDouble": "$total"} },
                    }
                }
            ]);
        newSettings.totalApproved = tr.length ? tr[0].total : 0;

    const IsExist = await Settings.find(newSettings).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'No changes Made',
        });
    }
   
    newSettings.updated_at = new Date();
    const updateObj = newSettings;
    const Id = req.params.Id;
    Settings.findByIdAndUpdate(Id, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

router.post('/status', auth.required, async (req, res, next) => {
    
    const { body: { loggedInUserData } } = req;
    let query = {};

    const starts = moment().startOf('day').toString();
    const ends = moment().endOf('day').toString();
    query.updated_at = {
        $gte: new Date(starts),
        $lte: new Date(ends)
    };

    query.user_id = loggedInUserData._id;
    
    Settings.find(query).sort({ updated_at: -1 }).then( async (d) => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    });
    
   
});


module.exports = router;