const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Users = mongoose.model('Users');
const Banks = mongoose.model('Banks');
const BankList = mongoose.model('BankList');
const Currency = mongoose.model('Currency');
const config = require('../config/config');
const CryptoJS = require("crypto-js");

// Add Bank
router.post('/', auth.required, async (req, res, next) => {
    const { body: {
        account_number,
        bank_name,
        holder_name,
        bank_address,
        bank_code,
        country,
        currency_id,
        status,
        file,
        bankType
    }
    } = req;

    if (!account_number || !bank_name || !holder_name || !bank_address || !bank_code || !country || !currency_id || !bankType) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const newBank = {};
    newBank.account_number = account_number;
    newBank.bank_name = bank_name;
    newBank.bank_code = bank_code;
    newBank.country = country;
    newBank.currency = currency_id;
    newBank.status = status
    newBank.logo = file;
    newBank.type = bankType;
    
    const IsExist = await Banks.find(newBank).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Bank already exist ',
        });
    }
    newBank.holder_name = holder_name;
    newBank.bank_address = bank_address;
    newBank.status = status ? true: false;
    newBank.created_at = new Date();
    newBank.updated_at = new Date();
    const finalBank = new Banks(newBank);
    return finalBank.save()
        .then((d) => {
            res.json({
                status: true,
                data: { d },
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
});
router.get('/active',  async (req, res, next) => {
    var code = req.query.code; 
    var type = req.query.type ? req.query.type : 'payin';
    const currencyAv = await Currency.find({currency_code: code}).select('_id').then(d => d).catch(e => { return false });
    if(!currencyAv){
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    } else {
    Banks.find({status:true, type: type, currency: { $in :currencyAv }}).limit(2).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
    }
    
   
});

router.get('/search',  async (req, res, next) => {
    var code = req.query.code; 
    var type = req.query.type ? req.query.type : 'payin';
    console.log('code',code)
    const currencyAv = await Currency.find({currency_code: code}).select('_id').then(d => d).catch(e => { return false });
    if(!currencyAv){
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    } else {
        try{
            const payin = await Banks.find({type: 'payin', currency: { $in :currencyAv }}).sort({status: -1});
            const payout = await Banks.find({type: 'payout', currency: { $in :currencyAv }}).sort({status: -1});
            
            return res.status(200).json({
                status: true,
                data: {payin: payin, payout: payout},
            });
        } catch(err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }
    }
    
   
});

// Get banks
router.get('/', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const c = await Currency.aggregate( [
         { $group : { _id : "$country", currency: { $push: "$$ROOT"}  } } ] ).then(c =>  c );
    const banks = await Banks.find().then(d =>d);
        return res.status(200).json({
            status: true,
            data: {banks: banks, currency: c},
        });
   
});

// Add Customer Bank 
router.post('/customer/bank', async (req, res, next) => {
    console.log('post data recieved', req.body);
    const { name, code, iban, country, status } = req.body;
    if (!name || !code || !iban || !country ) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const newBankList = {};
    newBankList.name = name;
    newBankList.code = code;
    newBankList.iban = iban;
    newBankList.country = country;
    newBankList.status = status

    const IsExist = await BankList.find(newBankList).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Bank already exist ',
        });
    }
    newBankList.status = status ? true: false;
    newBankList.created_at = new Date();
    newBankList.updated_at = new Date();
    const finalBank = new BankList(newBankList);
    return finalBank.save()
        .then((d) => {
            res.json({
                status: true,
                data: { d },
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
   
});

// Get Customer Banks
router.get('/customer/bank', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const banks = await BankList.find().then(d =>d);
        return res.status(200).json({
            status: true,
            data: {banks: banks},
        });
        
});

// Delete Customer Bank 
router.delete('/customer/bank/:bankId', auth.required, async (req, res, next) => {

    const bankId = req.params.bankId;
    console.log('bankId', bankId);
    BankList.findByIdAndRemove(bankId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});

// Update Customer Bank 
router.put('/customer/bank/:bankId', auth.required, async (req, res, next) => {
    const { name, code, iban, country, status } = req.body;
    const bankId = req.params.bankId;

    if (!name || !code || !iban || !country ) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    
    const newBankList = {};
    newBankList.name = name;
    newBankList.code = code;
    newBankList.iban = iban;
    newBankList.status = status;
    newBankList.country = country;
   
    
    const IsExist = await BankList.find(newBankList).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'No changes made',
        });
    }
    newBankList.updated_at = new Date();
    
    const updateObj = newBankList;
    const finalBank = new BankList(newBankList);
    BankList.findByIdAndUpdate(bankId, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});


router.get('/list', async (req, res, next) => {
    const { country } = req.body;
    let query = {};
    if(country && country != '') {
        const bytes = CryptoJS.AES.decrypt(country, config.secretAes);
        const plaintext = bytes.toString(CryptoJS.enc.Utf8);
        if(plaintext != "")
            query['country'] = plaintext.toUpperCase()
    }

    return BankList.find(query).then(dbBanks => {
        let banks = dbBanks.map(b => b.toObject());

        const str = JSON.stringify(banks);
        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
        return res.status(200).json({
            status: true,
            data: banks
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            error: (CryptoJS.AES.encrypt(err.toString(), config.secretAes)).toString()
        });
    });
});

router.get('/:bankId', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const bankId = req.params.bankId;

    Banks.findById(bankId).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

// update BANK
router.put('/:bankId', auth.required, async (req, res, next) => {
    const { body: {
        account_number,
        bank_name,
        holder_name,
        bank_address,
        bank_code,
        country,
        currency_id,
        status,
        file,
        bankType
    }
    } = req;
    const bankId = req.params.bankId;
    
    if (!account_number || !bank_name || !holder_name || !bank_address || !bank_code || !country || !currency_id || !bankType ) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const newBank = {};
    newBank.account_number = account_number;
    newBank.bank_name = bank_name;
    newBank.bank_code = bank_code;
    newBank.country = country;
    newBank.currency = currency_id;
    newBank.holder_name = holder_name;
    newBank.bank_address = bank_address;
    newBank.status = status? true: false;
    newBank.logo = file;
    newBank.type = bankType;
    
    const IsExist = await Banks.find(newBank).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'No changes made',
        });
    }
    newBank.updated_at = new Date();
    const updateObj = newBank;
    const finalBank = new Banks(newBank);
    Banks.findByIdAndUpdate(bankId, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

// Delete Bank
router.delete('/:bankId', auth.required, async (req, res, next) => {

    const bankId = req.params.bankId;
    Banks.findByIdAndRemove(bankId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});


module.exports = router;