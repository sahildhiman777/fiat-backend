const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Customer = mongoose.model('Customer');

router.post('/', auth.required, async (req, res, next) => {
    const { body: {
        email,
        phone,
        status,
    }
    } = req;

    if ( !email || !phone) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const c = {};
    c.email = email;
    c.phone = phone;
    
    const IsExist = await Customer.find(c).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Customer already exist ',
        });
    }
    c.status = status;
    c.created_at = new Date();
    const curr = new Customer(c);
    return curr.save()
        .then(() => {
            res.json({
                status: true,
                data:  c ,
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
});




router.post('/get', auth.required, async (req, res, next) => {
    const { payload: { id, } } = req;  
    let query = {}
    
    let pageNum = req.body.pageNumber;
    let nPerPage = 25;
    
    const d = await Customer.find(query).sort( { updated_at: -1 } ).skip( pageNum > 0 ? ( ( pageNum - 1 ) * nPerPage ) : 0 ).limit( nPerPage ).populate('user_id', '_id name role email').then(d => d).catch((err => { }));
    const t = await Customer.find(query).then(d =>d);
    const total = t.length

    const obj = {
        data:  d,
        total: total
     }

    return res.status(200).json({
        status: true,
        data: obj
    });
});

router.get('/:cId', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const cId = req.params.cId;

    Customer.findById(cId).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.put('/:cId', auth.required, async (req, res, next) => {
    const { body: {
        email,
        phone,
        status,
    }
    } = req;

    if ( !email || !phone) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const c = {};
    c.email = email;
    c.phone = phone;
    c.status = status;
    
    const IsExist = await Customer.find(c).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'No changes made',
        });
    }
    c.updated_at = new Date();
    const updateObj = c;
    const cId = req.params.cId;
    Customer.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

router.delete('/:cId', auth.required, async (req, res, next) => {

    const cId = req.params.cId;
    Customer.findByIdAndRemove(cId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});


module.exports = router;