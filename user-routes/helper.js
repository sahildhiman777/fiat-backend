const mongoose = require('mongoose');
const Transaction = mongoose.model('Transaction');
const moment = require('moment');
var momentTZ = require('moment-timezone');
const config = require('../config/config');

module.exports = {
    aggrecate: async function (deposit, time, type, currency_code) {
        // console.log('deposit',deposit)
        // console.log('time',time)
        // console.log('type',type)
        let start = '2000-01-01 ';
        if (time === 'week') {
            start = moment().subtract(7, 'day').toString();
        } else if (time === 'month') {
            start = moment().startOf('month').toString();
        }
        else if (time === 'daily') {
            start = moment().startOf('day').toString();
        }

        const tonight = moment().endOf('day').toString();
        const d = await Transaction.aggregate([
            {
                $match: {
                    created_at: {
                        $gte: new Date(start),
                        $lte: new Date(tonight)
                    },
                    deposit: deposit,
                    //status: 'approved'
                    currency_code: currency_code
                }
            },

            {

                $group: {
                    _id: type,
                    sum: {
                        $sum: `$${type}`
                    }
                }
            }
        ]
        ).then(d => {
            //console.log(type,d)
            return d;
        }).catch(err => {
            return;
        });
        return d;
    },
    groupedSum: async function (deposit = false, dashboard = true, time = 'month', currency_code) {
        const tonight = moment().endOf('day').toString();
        let start = moment().startOf('year').toString();
        let group = { $month: "$created_at" };
        if (time == 'week') {
            start = moment().startOf('month').toString();
            group = { $week: "$created_at" };
        } else if (time == 'day') {
            start = moment().startOf('month').toString();
            group = { $dayOfMonth: "$created_at" };

        } else if (time == 'hour') {
            start = moment().startOf('day').toString();
            //group ={ $hour: "$created_at" };
            group = { $floor: { $divide: [{ $hour: "$created_at" }, 2] } };
        }

        let match = {};
        if (dashboard) {
            match = {
                created_at: {
                    $gte: new Date(start),
                    $lte: new Date(tonight)
                },
                deposit: deposit,
                status: 'approved',
                currency_code: currency_code
            };
        } else {
            match = {
                created_at: {
                    $gte: new Date(start),
                    $lte: new Date(tonight)
                },
                deposit: deposit,
            };
        }

        // console.log('match',match)
        // console.log('groupt',group)
        const d = await Transaction.aggregate([
            {
                $match: match
            },

            {
                $group: {
                    _id: group,
                    total: { $sum: "$total" },
                }
            },
        ]
        ).then(d => {

            return d;
        }).catch(err => {
            //console.log(err)
            return;
        });
        return d;
    },

    limit: async function (deposit = false, user = true, email = null) {
        if (user) {
            const start = moment().startOf('day').toString();
            const tonight = moment().endOf('day').toString();
            const d = await Transaction.aggregate([
                {
                    $match: {
                        created_at: {
                            $gte: new Date(start),
                            $lte: new Date(tonight)
                        },
                        deposit: deposit,
                        status: 'approved',
                        email: email
                    }
                },

                {

                    $group: {
                        _id: total,
                        sum: {
                            $sum: `$total`
                        }
                    }
                }
            ]
            ).then(d => {
                return d;
            }).catch(err => {
                return 0;
            });
            return d;
        } else {
            const start = moment().startOf('day').toString();
            const tonight = moment().endOf('day').toString();
            const d = await Transaction.aggregate([
                {
                    $match: {
                        created_at: {
                            $gte: new Date(start),
                            $lte: new Date(tonight)
                        },
                        deposit: deposit,
                        status: 'approved'
                    }
                },

                {

                    $group: {
                        _id: total,
                        sum: {
                            $sum: `$total`
                        }
                    }
                }
            ]
            ).then(d => {
                return d;
            }).catch(err => {
                return 0;
            });
            return d;
        }

    },


    transactions: async function (deposit, status, platform, currency, searchText, pn) {
        if(status || platform || currency || searchText || pn){
            let query = {};
            let pageNumber = pn;
            let nPerPage = 25;
            query.deposit = deposit;
            
            if(status) {
                query.status = status;
            }

            if(platform) {
                query.platform = new RegExp(["^", platform, "$"].join(""), "i");
            }

            if(currency) {
                if(deposit){
                    query.currency_code = currency;
                } else {
                    query.user_currency_code = currency;
                }
            }

            if (searchText) {
                query.$or = [
                    { "user_id": { $regex: '.*' + searchText + '.*' } },
                    { "email": { $regex: '.*' + searchText + '.*' } },
                    { "transaction_id": { $regex: '.*' + searchText + '.*' } },
                    { "coin_name": { $regex: '.*' + searchText + '.*' } },
                    { "coin_address": { $regex: '.*' + searchText + '.*' } },
                    { "transaction_hash": { $regex: '.*' + searchText + '.*' } },
                    { "payus_transaction_id": { $regex: '.*' + searchText + '.*' } },
                    { "phone": { $regex: '.*' + searchText + '.*' } }
                ];
            }
            
            const d = await Transaction.find(query).sort( { updated_at: -1 } ).skip( pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0 ).limit( nPerPage ).populate('bank').populate('edited_by', '_id name role email').then(d => d).catch((err => { }));
            
            const t = await Transaction.find(query).sort( { updated_at: -1 } ).then(d => d).catch((err => { }));
            
            return { data: d, total: t.length };
        }else {
            const d = await Transaction.find({deposit: deposit}).sort( { updated_at: -1 } ).populate('bank').populate('edited_by', '_id name role email').then(d => d).catch((err => { }));
            return d;
        }
    },
    transactionsSearch: async function (deposit, start, end, cur, input, status, bank, time, timezone, loggedInUserData) {

        const starts = start ? moment(start, 'DD-MM-YYYY').startOf('day').toString() : moment().startOf('day').toString();
        const ends = end ? moment(end, 'DD-MM-YYYY').endOf('day').toString() : moment().endOf('day').toString();
        const currency_code = cur ? cur : false;
        const bank_id = bank ? bank : false;
        const search = input ? input : false;
        const type = deposit ? deposit: false;        
        status = status ? status : false;
        let dateType = "$created_at";
        let query = {};
        
        if(time === 'all') {
            if(status !== false && status !== 'pending' && type !== 'buy'){
                dateType = "$edited_at";
            }
        } else  {            
            if(status === false || status === 'pending' || type === 'buy'){
                query.created_at = {
                    $gte: new Date(starts),
                    $lte: new Date(ends)
                };
            }else{
                query.edited_at = {
                    $gte: new Date(starts),
                    $lte: new Date(ends)
                };
                dateType = "$edited_at";
            }
        }
        
        if (currency_code) {
            query.$or = [{ currency_code: currency_code }, { user_currency_code: currency_code }, { user_currency: currency_code }];

        }

        if (search) {
            query.$or = [
                { "user_id": { $regex: '.*' + search + '.*' } },
                { "email": { $regex: '.*' + search + '.*' } },
                // { "username": { $regex: '.*' + search + '.*' } }
            ];
        }
        
        if (bank_id) {
            query.bank = mongoose.Types.ObjectId(bank_id);
        }
        
        if (status) {
            query.status = status;

            if(type === 'sell' && loggedInUserData && loggedInUserData.role != "superadmin" && (status === 'approved' || status === 'rejected')){
                query.edited_by = mongoose.Types.ObjectId(loggedInUserData._id);
            }
        }
        if (type){
            query.deposit = type === 'buy' ? true : false;
        }
        
        const grouped = await Transaction.aggregate([{ $match: query }, { $sort : { updated_at : -1 } }, 
        {
            $group: {
                _id: { 
                    // year: { $year:"$created_at" },
                    // month: { $month:"$created_at" },
                    // // week: { $week: "$created_at" }
                    // day:{$dayOfMonth:"$created_at"}
                    "$dateToString": {"format": "%d-%m-%Y", "date": dateType, "timezone": timezone}
                 },
                total: { $sum: {"$toDouble": "$total"} },
            }
        },]
        ).then(h => h).catch(e => {console.log('err',e)});
       
        const tr = await Transaction.find(query).populate('bank').populate('edited_by', '_id name role email').sort({ updated_at: -1 }).then(d => d).catch((err => { }));
        const toReturn= {
            tr,
            grouped
        }
        
        return toReturn;
    },
    notify: async function (data) {
        const url = config.notify_url;

        var body = JSON.stringify(data);

        fetch(url, { method: "POST", body: body })
            .then(response => {
                if (response.ok) {

                    // console.log('response',response)
                } else {
                    log.error(`notificationFailed for address  ${user.crypto_address} at ${url} error ${response}`);
                    //console.log(response);
                }
            }).
            catch(error => {
                console.log(error);
            });

        return true;
    }

}
