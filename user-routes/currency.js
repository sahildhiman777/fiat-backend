const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Currency = mongoose.model('Currency');
var request = require("request");
const axios = require('axios');

// TO-DO: Make it dynamic later by adding coinlist in db
const coin_list = [{
    'name': 'bitcoin',
    'symbol': 'BTC',
    'icon': 'BTCUSDT',
    'buy_status': true,
    'sell_status': true
},
{
    'name': 'ethereum',
    'symbol': 'ETH',
    'icon': 'ETHBTC',
    'buy_status': true,
    'sell_status': true
},
{
    'name': 'sonicex',
    'symbol': 'SOX',
    'icon': 'SOX',
    'buy_status': true,
    'sell_status': true
},
{
    'name': 'digitalusd',
    'symbol': '$USD',
    'icon': 'USD',
    'buy_status': true,
    'sell_status': true
},
{
    'name': 'tether',
    'symbol': 'USDT',
    'icon': 'usdt',
    'buy_status': false,
    'sell_status': true
}];

const global_coins = ['BTC', 'ETH', 'USDT'];
const sox_coins = ['SOX', '$USD']


router.get('/get_data', async (req, res, next) => {
    let symboldata = [];
    let dataary = [
        { symbol: 'ETHBTC' },
        { symbol: 'DASHBTC' },
        { symbol: 'TRXBTC' },
        { symbol: 'BCHBTC' },
        { symbol: 'LTCBTC' },
        { symbol: 'XRPBTC' },
        { symbol: 'ADABTC' },
        { symbol: 'DOGEBTC' },
        { symbol: 'EOSBTC' },
        { symbol: 'VETBTC' },
        { symbol: 'XLMBTC' },
        { symbol: 'BTCUSDT' },
    ]
    try {
        let options = {
            method: "GET",
            url:
                `https://api.binance.com/api/v3/ticker/24hr`
        };
        await request(options, (err, response, body) => {
            if (err) {
                return res.json({
                    status: "error",
                    message: err,
                })
            } else {
                let btcData = JSON.parse(body);
                btcData.forEach(async (element) => {
                    for (let data of dataary) {
                        if (element.symbol == data.symbol) {
                            await symboldata.push(element);
                        }
                    }
                });
                if (symboldata.length > 0) {

                    return res.json({
                        status: "succes",
                        message: symboldata,
                        // message: body,
                    })
                }
            }
        });
    } catch (err) {
        return res.json({
            status: "error",
            message: err,
        })
    }
});

//`https://free.currconv.com/api/v7/convert?q=${curr_usd}&compact=ultra&apiKey=f989a49619de104dacbe`,
router.get('/get_wallets', async (req, res, next) => {
    let symboldata = [];
    let currency = req.query.currency || 'vnd';
    let curr_usd = `USD_${currency.toUpperCase()}`;
    let sox_data, curr_usd_rate, baseSoxPair;
    let external_apis = [
        'http://api.sonicex.com/FrontAPI/api/Transaction/GetTradePairAsset',
        `https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&ids=tether`
    ];

    try {
        let external_api_ary = external_apis.map(url => {
            return new Promise(async (resolve, reject) => {
                axios.get(url).then(res => {
                    return resolve(res.data);
                }).catch(err => {
                    console.log('Error fetching data', err);
                    return resolve(false);
                });
            })
        });

        return Promise.all(external_api_ary).then(data => {
            sox_data = data[0];
            curr_usd_rate = data[1][0] && data[1][0].current_price;
            baseSoxPair = sox_data.response && sox_data.response.find(basePair => basePair.BaseCurrencyName == 'SONICX USD DIGITAL')
            
            let promiseArray = coin_list.map(coinObj => {
                return new Promise(async (resolve, reject) => {
                    let ticker_data = {};
                    ticker_data = {
                        "current_price": 0,
                        "high_24h": 0,
                        "low_24h": 0
                    }

                    if (global_coins.some(coin => coin == coinObj.symbol)) {
                        let api_res = await axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&ids=${coinObj.name}`).catch(err => {
                            console.log(`Error gettng price for ${currency}/${coinObj.symbol}`, err);
                        });
                        api_res = api_res.data;

                        if (api_res.length > 0) {
                            ticker_data = api_res[0];
                        }

                        return resolve({
                            ...coinObj,
                            price: ticker_data.current_price,
                            lowPrice: ticker_data.low_24h,
                            highPrice: ticker_data.high_24h
                        });
                    } else if (sox_coins.some(coin => coin == coinObj.symbol)) {
                        let usd_rate = 1;
                        ticker_data = {
                            "CurrentRate": 0,
                            "Low52Week": 0,
                            "HighWeek": 0
                        }

                        if (curr_usd_rate > 0) {
                            usd_rate = curr_usd_rate;
                        }

                        if (coinObj.symbol == '$USD') {
                            return resolve({
                                ...coinObj,
                                price: usd_rate,
                                lowPrice: usd_rate,
                                highPrice: usd_rate
                            });
                        } else {
                            let api_res = baseSoxPair && baseSoxPair.PairList.find(pair => pair.PairName == `${coinObj.symbol}_USD`);
                            if (api_res) {
                                ticker_data = api_res;
                            }

                            return resolve({
                                ...coinObj,
                                price: (Number(ticker_data.CurrentRate) * usd_rate).toFixed(6),
                                lowPrice: (Number(ticker_data.Low52Week) * usd_rate).toFixed(6),
                                highPrice: (Number(ticker_data.HighWeek) * usd_rate).toFixed(6)
                            });
                        }
                    } else {
                        return resolve(false);
                    }
                });
            });

            return Promise.all(promiseArray)
        }).then(data => {
            symboldata = data.filter(d => d !== false);

            return res.json({
                status: "succes",
                message: symboldata,
            })
        }).catch(error => {
            console.log(error);
            return res.json({
                status: "error",
                message: error,
            })
        })
    } catch (error) {
        console.log(error);
        return res.json({
            status: "error",
            message: error,
        })
    }
});

router.post('/', auth.required, async (req, res, next) => {
    const { body: {
        country,
        currency,
        currency_code,
        file
    }
    } = req;

    if (!country || !currency || !currency_code) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const c = {};
    c.country = country.toUpperCase();
    c.currency = currency;
    c.currency_code = currency_code.toUpperCase();
    c.logo = file

    const IsExist = await Currency.find(c).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Currency already exist ',
        });
    }
    const curr = new Currency(c);
    return curr.save()
        .then(() => {
            res.json({
                status: true,
                data: c,
            })
        })
        .catch((err) => res.json({
            status: false,
            data: err.toString(),
        }));
});


router.get('/', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    Currency.find().then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.get('/:cId', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const cId = req.params.cId;

    Currency.findById(cId).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.put('/:cId', auth.required, async (req, res, next) => {
    const { body: {
        country,
        currency,
        currency_code,
        file
    }
    } = req;

    if (!country || !currency || !currency_code) {
        return res.status(200).json({
            status: false,
            data: 'All Fields are required',
        });
    }
    const cId = req.params.cId;
    const c = {};
    c.country = country.toUpperCase();
    // c.currency = currency;
    c.logo = file;
    c.currency_code = currency_code.toUpperCase();

    const IsExist = await Currency.find({ c, _id: { $ne: cId }, }).then((data) => (data));
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            data: 'Currency exist with same country and Currency Code',
        });
    }
    c.currency = currency;
    const updateObj = c;

    Currency.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
        if (model) {
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

router.delete('/:cId', auth.required, async (req, res, next) => {

    const cId = req.params.cId;
    Currency.findByIdAndRemove(cId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});


module.exports = router;