const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('./auth');
const Users = mongoose.model('Users');
const Userwallets = mongoose.model('Userwallets');
const userKyc = mongoose.model('userKyc');
const Coins = mongoose.model('Coins');
const otpModel = mongoose.model('Otp');
var speakeasy = require('speakeasy');
var validator = require("email-validator");
var jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mail = require('./mail');
var nodemailer = require('nodemailer');
const config = require('../config/config')
const client = require('twilio')(config.mobile_smtp.accountSid, config.mobile_smtp.authToken);
const axios = require('axios');
var path = require("path");
const fs = require('fs');
const PayusAPI = require('@payus/payus-sdk');

const multer = require('multer');
var storage = multer.diskStorage(
    {
        destination: function (req, file, cb) {
            cb(null, '/var/www/html/upload/tmp');
        },
        filename: function (req, file, cb) {
            const fileName = file.originalname,
                filePath = path.parse(fileName),
                fileExtension = (filePath.ext).toLowerCase(),
                uniqueFileName = `${new Date().getTime()}${fileName}`;

            cb(null, `${uniqueFileName}`);
        }
    }
);
var upload = multer({ storage: storage });
var smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'processorpayment2017@gmail.com',
        pass: 'riegtfcvigcgyhgl',
    },
});

const createWallet = async (user) => {
    Coins.find({}).then(coinList => {
        if (coinList && coinList.length > 0) {
            for (let i = 0; i < coinList.length; i++) {
                Userwallets.findOne({
                    coin_code: coinList[i].symbol,
                    user_id: user._id
                }).then(wallet => {
                    let coin_code = coinList[i].symbol;

                    if (coinList[i].symbol == "$USD")
                        coin_code = "SRC20-USD";
                    else if (coinList[i].symbol == "USDT")
                        coin_code = "ERC20-USDT"

                    if (!wallet) {
                        const payus = new PayusAPI(config.payusAccessToken);
                        var payusResult = payus.paymentCryptoRest({
                            currency: "USD",
                            return_url: "http://www.testurl.com/nofiy",
                            notify_url: "http://www.testurl.com/nofiy",
                            coin_code: coin_code,
                            product_amount: "1",
                            invoice_id: new Date().getTime(),
                            email: user.email,
                        });

                        payusResult.then((res) => {
                            let saveData = {
                                user_id: user._id,
                                coin_name: coinList[i].name,
                                coin_code: coinList[i].symbol,
                                coin_balance: 0,
                                converted_balance: 0,
                                currency_rate: res.data.result.product_amount_base,
                                icon: coinList[i].icon,
                                status: true,
                                buy_status: coinList[i].buy_status,
                                sell_status: coinList[i].sell_status,
                                coin_address: res.data.result.crypto_address,
                                destination_tag: res.data.result.destination_tag,
                                crypto_address_json_resp: res.data.result.crypto_address_json_resp,
                                created_date: new Date().getTime(),
                                modified_date: new Date().getTime(),
                            }
                            let savedwallets = new Userwallets(saveData);
                            savedwallets.save()
                        })
                            .catch((err) => {
                                console.log("ERROR: ====", err);
                            })
                    }
                }).catch(console.log);
            }
        }
    }).catch(console.log)
}

router.post('/kycimage', auth.optional, upload.single('file'), async (req, res, next) => {
    let filename = req.file.filename;
    let kyc_level = req.body.kyc_level;
    let id = req.body.id;
    let source_path = path.resolve('/var/www/html/upload/tmp/');
    let destination_path = path.resolve('/var/www/html/upload/');
    fs.mkdir(destination_path, { recursive: true }, (err) => {
        if (err) throw err;
        fs.rename(path.resolve(source_path, filename), path.resolve(destination_path, kyc_level + id + filename), function (err) {
            if (err) {
                return console.error(err);
            }
        });
    });

    try {
        let update_detail;
        if (kyc_level == 'kyc_level2') {
            update_detail = { kyc_level2: 'pending' };
        }
        if (kyc_level == 'kyc_level3') {
            update_detail = { kyc_level3: 'pending' };

        }
        let kyc_data = {
            user_id: req.body.id,
            level: req.body.kyc_level,
            status: 'pending',
            kyc_doc: kyc_level + id + filename,
        }
        let savedKyc = new userKyc(kyc_data);

        savedKyc.save().then(() => {
            return Users.findOneAndUpdate({ _id: id }, { $set: update_detail });
        }).then((user) => {
            if (user != null) {
                return res.json({
                    status: "success",
                    message: 'Upload successfully',
                    data: user
                })
            } else {
                return res.json({
                    status: "failed",
                    message: 'Upload failed'
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
});

router.post('/register', auth.optional, async (req, res, next) => {
    const { body: { email, password, firstname, lastname } } = req;
    const coinList = req.body.coinList;

    if (!email) {
        return res.status(200).json({
            status: false,
            message: 'Email is required',
            data: [],
        });
    }

    if (!password) {
        return res.status(200).json({
            status: false,
            message: 'Password is required',
            data: [],
        });
    }
    if (!firstname) {
        return res.status(200).json({
            status: false,
            message: 'First name is required.',
            data: [],
        });
    }
    if (!lastname) {
        return res.status(200).json({
            status: false,
            message: 'Last name is required.',
            data: [],
        });
    }
    const user = {};
    user.email = email;
    user.password = password;
    user.role = 'user';
    user.firstname = firstname;
    user.lastname = lastname;
    user.name = firstname + ' ' + lastname;
    const secret = speakeasy.generateSecret({ length: 20 });
    user.faKey = secret.base32;
    user.otpauth_url = secret.otpauth_url;
    const finalUser = new Users(user);
    const IsExist = await Users.find({ email: user.email }).then((data) => (data));

    if (!validator.validate(user.email)) {
        return res.status(200).json({
            status: false,
            message: 'Email is not valid.',
            data: [],
        });
    }
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            message: 'Email is already exists.',
            data: []
        });
    }
    finalUser.setPassword(user.password);
    const newUser = await finalUser.save().catch((err) => res.json({
        status: false,
        message: err.toString(),
        data: []
    }));

    if (newUser) {
        await createWallet(newUser)

        return res.json({
            status: true,
            message: 'User is registered successfully.',
            data: newUser,
        })
    }

});

//POST login route (optional, everyone has access)
router.post('/login', auth.optional, (req, res, next) => {
    const { body: { email, password } } = req;

    const user = {};
    user.email = email;
    user.password = password;
    if (!user.email) {
        return res.status(200).json({
            status: false,
            message: 'Email is required.',
            data: []
        });
    }

    if (!user.password) {
        return res.status(200).json({
            status: false,
            message: 'Password is required.',
            data: []
        });
    }

    return passport.authenticate('local', { session: false }, async (err, passportUser, info) => {
        if (err) {
            return res.status(200).json({
                status: false,
                message: 'Credentials are invalid.',
                data: []
            });
        }

        if (passportUser && passportUser.role === 'user') {
            const user = passportUser;
            await createWallet(user);
            user.token = passportUser.generateJWT();
            return res.json({
                status: true,
                message: 'User is logged in successfully.',
                data: { user: user.toAuthJSON() }
            });

        }
        return res.status(200).json({
            status: false,
            message: 'Credentials are invalid.',
            data: []
        });
    })(req, res, next);
});

//Change user name and password
router.put('/changepassword', auth.required, async (req, res, next) => {
    const { password } = req.body;
    const newPass = password
    const userId = req.body.id;

    Users.findOne({ _id: userId })
        .then(user => {
            // console.log('user is finded', user);
        });

    const newUser = {}

    if (!newPass) {
        // newUser.name = name;
    }
    else {
        const salt = this.salt = crypto.randomBytes(16).toString('hex');
        const hash = this.hash = crypto.pbkdf2Sync(newPass, this.salt, 10000, 512, 'sha512').toString('hex');

        // newUser.name = name;
        newUser.salt = salt;
        newUser.hash = hash;
    }
    Users.findByIdAndUpdate({ _id: userId }, { $set: newUser }).then(changed => {
        try {
            return res.json({
                status: 200,
                message: "Password changed successfully",
                data: changed
            })
        } catch (err) {
            return res.json({
                status: "error",
                message: err,
            })
        }
    });
})

//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, (req, res, next) => {
    const { payload: { id } } = req;

    return Users.findById(id)
        .then((user) => {
            if (!user) {
                return res.status(200).json({
                    status: false,
                    message: 'Session have been expired!',
                    data: []
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Session data is fetched successfully.',
                data: { user: user.toAuthJSON() }
            });
            // return res.json({ user: user.toAuthJSON() });
        });
});


router.get('/', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    Users.find({ _id: { $ne: id } }).select(['id', 'name', 'firstname', 'lastname', 'role', 'email', 'faKey']).then(d => {
        return res.status(200).json({
            status: true,
            message: 'User data is fetched successfully.',
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            message: err.toString(),
            data: []
        });
    });
});

router.get('/profile', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;

    Users.findById(id).select(['id', 'name', 'firstname', 'lastname', 'role', 'email', 'faKey']).then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.put('/profile', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const { body: { firstname, lastname, email } } = req;
    const IsExist = await Users.find({ email: email, _id: { $ne: id } }).then((data) => (data));
    if (!validator.validate(email)) {
        return res.status(200).json({
            status: false,
            message: 'Email is not valid.',
            data: [],
        });
    }
    if (IsExist.length > 0) {
        return res.status(200).json({
            status: false,
            message: 'Email is already exists. ',
            data: []
        });
    }
    const name = firstname + ' ' + lastname;
    const updateObj = { name, firstname, lastname, email };
    Users.findByIdAndUpdate(id, updateObj, { new: true }, function (err, model) {
        if (model) {
            model.salt = false;
            model.hash = false;
            return res.status(200).json({
                status: true,
                message: 'Profile has been updated successfully.',
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                message: err.toString(),
                data: []
            });
        }

    });
});

router.delete('/', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    Users.findByIdAndRemove(id, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                message: error.toString(),
                data: []
            });
        } else {
            return res.status(200).json({
                status: true,
                message: 'Profile has been deleted successfully.',
                data: [],
            });

        }
    });
});

router.get('/2fa/:adminid', auth.required, async (req, res, next) => {

    const adminId = req.params.adminid;
    // Generate a secret, if needed
    var secret = speakeasy.generateSecret({ length: 20 });
    // By default, generateSecret() returns an otpauth_url for SHA1
    // Use otpauthURL() to get a custom authentication URL for SHA512
    var url = speakeasy.otpauthURL({ secret: secret.ascii, label: 'ExchangeFiat', algorithm: 'sha512' });
    const updateObj = { faKey: secret.base32, otpauth_url: url, google2fa: false };

    Users.findByIdAndUpdate(adminId, updateObj, { new: true }, function (err, model) {
        if (model) {
            model.salt = false;
            model.hash = false;
            mail.otp(model);
            return res.status(200).json({
                status: true,
                data: model,
            });
        }
        else if (err) {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        }

    });
});

//Varify Tokken
router.post('/verifytokken', auth.optional, (req, res, next) => {
    const { token } = req.body;
    const verify = jwt.verify(token, 'secret');
    Users.findOne({ email: verify.data.email })
        .then(user => {
            if (user === '') {
                return res.json({
                    status: "error",
                    message: "User not found!",
                });
            } else {
                return res.json({
                    status: "success",
                    userInfo: user
                });

            }
        });
})
//Forgot Password
router.post('/forgotpassword', auth.optional, (req, res, next) => {
    const userEmail = req.body.email;
    Users.findOne({ email: userEmail })
        .then(user => {
            if (user) {
                const token_data = { 'email': req.body.email }
                var token = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60),
                    data: token_data
                }, 'secret');
                mail.forgotPassword({
                    name: user.firstname,
                    email: req.body.email,
                    link: config.BaseUrl + "/resetpassword?token=" + token,
                })
                return res.status(200).json({
                    status: "success",
                    message: "Email sent successfully",
                });
            }
            else {
                return res.json({
                    status: "error",
                    message: 'Email not registerd',
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
});

router.post('/resetpassword', auth.optional, async (req, res, next) => {
    const { name, password } = req.body;
    const newPass = password
    const userId = req.body.id;
    Users.findOne({ _id: userId })
        .then(user => {
            // console.log('user is finded', user);
        });
    const newUser = {}
    if (!newPass) {
        newUser.name = name;
    }
    else {
        const salt = this.salt = crypto.randomBytes(16).toString('hex');
        const hash = this.hash = crypto.pbkdf2Sync(newPass, this.salt, 10000, 512, 'sha512').toString('hex');
        newUser.name = name;
        newUser.salt = salt;
        newUser.hash = hash;
    }
    Users.findByIdAndUpdate({ _id: userId }, { $set: newUser }).then(changed => {
        try {
            return res.json({
                status: 200,
                message: "Password changed successfully",
                data: changed
            })
        } catch (err) {
            return res.json({
                status: "error",
                message: err,
            })
        }
    });
})
//mobile send otp--->
router.post('/sendotp', auth.optional, (req, res) => {
    try {
        let user_id = req.body.id
        let otp = Math.floor(100000 + Math.random() * 900000);
        let update_detail;
        if (req.body.phone) {
            update_detail = { mobile_Otp: otp, phone: req.body.phone };
        } else {
            update_detail = { mobile_Otp: otp };
        }
        Users.findOneAndUpdate({ _id: user_id }, { $set: update_detail }).then((user) => {
            if (user) {
                client.messages.create({
                    body: ` Exchange fiat has been send an OTP: ` + otp + `. OTP is confidential. For security reasons,don't share this OTP with anyone.`,
                    from: '+12563715464',
                    to: req.body.phone
                }).then(message => {
                    if (message) {
                        return res.json({
                            status: "success",
                            message: "Verifiy your otp.",
                            // data: otp_res
                        });
                    }

                }).catch((err) => {
                    return res.json({
                        status: "error",
                        message: err
                    });
                });
            }
            else {
                return res.json({
                    status: "error",
                    message: 'mobile is not found',
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
})

router.post('/send2fa_email', auth.optional, async (req, res, next) => {
    rand = Math.floor(100000 + Math.random() * 900000);
    let email_otp = { email_2facode: rand }

    let name = req.body.name;
    Users.findOneAndUpdate({ email: req.body.email }, { $set: email_otp }).then((user) => {
        if (user) {

            link = config.BaseUrl + "/verify/" + req.body.id + "/" + rand;
            // link = "http://localhost:3000/verify/" + req.body.id + "/" + rand;
            let mailOptions = {
                from: "support@giaodichcoin.com",
                to: req.body.email,
                subject: "2FA email verification",
                html: "<p style='font-size: 17px;     color: #222;'>Hello " + user.name + ",</p><p style='font-size: 17px;    color: #222;'>Welcome to ExchangeFiat.</p><p style='font-size: 17px;    color: #222;'>Below is your Email Verification Code.</p><p style='font-size: 17px;    color: #222;'>We may need to send you critical information about our service and it is important that we have an accurate email code.</p><p style='font-size: 17px;    color: #222;'>* Be sure to don't give your email code to anyone.</p><p style='font-size: 17px;    color: #222;'>Email Key Code :" + email_otp.email_2facode + " </p><p style='font-size: 17px;    color: #222;'>Thanks,</p><p style='font-size: 17px;    color: #222;'>ExchangeFiat Team</p>"
            }
            smtpTransport.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return res.json({
                        status: "error",
                        message: error,
                    })
                } else {
                    return res.json({
                        status: "success",
                        message: "Please check your email, OTP has been sent successfully",
                    })
                }
            });
            // let mailOptions = {
            //     from: "support@giaodichcoin.com",
            //     to: req.body.email,
            //     subject: "Please confirm your Email account ",
            //     html: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
            // }
            // smtpTransport.sendMail(mailOptions, function (error, info) {
            //     if (error) {
            //         return res.json({
            //             status: "error",
            //             message: error,
            //         })
            //     } else {
            //         return res.json({
            //             status: "success",
            //             message: "Email sent successfully",
            //         })
            //     }
            // });
        }
    }).catch(err => {
        res.json({
            status: 'failed',
            message: 'err'
        })
    });
});

//verifiy mobille otp------=->>
router.post('/verifiyotp', auth.optional, (req, res) => {
    try {
        let otp = req.body.otp;
        let user_id = req.body.id;
        let phone_status = { kyc_level1: 'approved' }
        Users.findOneAndUpdate({ _id: user_id, mobile_Otp: otp }, { $set: phone_status }).then((user) => {
            // Users.findOne({ _id: user_id, mobile_Otp: otp }).then((user) => {
            if (user != null) {
                return res.json({
                    status: "success",
                    message: 'OTP verifiy successfully'
                })
            }
            else {
                return res.json({
                    status: "failed",
                    message: 'OTP is invalid'
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
})

//email 2fa
router.post('/email_2fa', auth.optional, (req, res) => {
    try {
        const { body: { userToken } } = req;
        if (!userToken) {
            res.json({
                status: false,
                data: 'A valid OTP required',
            });
        }
        const authorization = req.headers.authorization;
        let token = '';
        if (authorization && authorization.split(' ')[0] === 'Bearer') {
            token = authorization.split(' ')[1];
        }

        const { payload: { id } } = req;
        if (!id) {
            return res.status(200).json({
                status: false,
                data: 'Please Login1',
                debug: 'token not found'
            });
        }
        Users.findOne({ _id: id, email_2facode: userToken }).then((user) => {
            if (user) {
                return res.status(200).json({
                    status: true,
                    data: user,
                });
            }
            else {
                return res.status(200).json({
                    status: false,
                    data: 'OTP is not valid',
                });
            }
        }).catch(err => {
            return res.json({
                status: false,
                data: 'please login2',
            })
        })
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
})

router.post('/disable2faemail', auth.optional, (req, res) => {
    try {
        let user_id = req.body.id;
        Users.findOneAndUpdate(
            { _id: user_id },
            {
                $set: { email_2fa: false }
            },
            { new: true }
        ).then((user) => {
            if (user != null) {
                return res.json({
                    status: "success",
                    message: 'Email 2FA has disabled successfully.',
                    data: user
                })
            }
            else {
                return res.json({
                    status: "failed",
                    message: 'Email is not disabled'
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
})

router.post('/enable2faemail', auth.optional, (req, res) => {
    try {
        let user_id = req.body.id;
        Users.findOneAndUpdate(
            { _id: user_id },
            {
                $set: { email_2fa: true, google2fa: false }
            },
            { new: true }
        ).then((user) => {
            if (user != null) {
                return res.json({
                    status: "success",
                    message: 'Email 2FA has enabled successfully.',
                    data: user
                })
            } else {
                return res.json({
                    status: "failed",
                    message: 'Email 2FA is not enabled'
                })
            }
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
});

router.post('/verifiy_2facode', auth.optional, (req, res) => {
    try {
        let user_id = req.body.id;
        let otp_code = req.body.code;
        let code_type = req.body.code_type;

        // Veifiy Google 2FA-------->>>
        if (code_type == 'google_2fa') {
            Users.findOne({ _id: user_id }).then(user => {
                var verified = speakeasy.totp.verify({
                    secret: user.faKey,
                    encoding: 'base32',
                    token: otp_code,
                    window: 10,
                });
                if (!verified) {
                    return res.json({
                        status: false,
                        data: 'Secret code is wrong.',
                    });
                }
                if (verified) {
                    return res.json({
                        status: 200,
                        message: "Google OTP has been verified successfully",
                        data: user
                    })
                }
            });
        }
        // Veifiy Email 2FA-------->>>

        if (code_type == 'email_2fa') {
            Users.findOne({ _id: user_id, email_2facode: otp_code }).then((user) => {
                if (user != null) {
                    return res.json({
                        status: "success",
                        message: 'Email OTP has been verified successfully',
                    })
                }
                else {
                    return res.json({
                        status: "failed",
                        message: 'invalid code'
                    })
                }
            }).catch(err => {
                return res.json({
                    status: "error",
                    message: err,
                })
            });
        }
    } catch (err) {
        console.log('worng');

        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong,Try after sometime',
        })
    }
})

router.post('/emailverifysendcode', auth.optional, async (req, res, next) => {
    rand = Math.floor((Math.random() * 100) + 54);
    let email_otp = { emailotp: rand }
    Users.findOneAndUpdate({ email: req.body.email }, { $set: email_otp }).then((user) => {
        if (user) {
            mail.emailVerify({
                name: user.firstname,
                email: req.body.email,
                link: config.BaseUrl + "/verify/" + user.id + "/" + rand,
            })
            return res.status(200).json({
                status: "success",
                message: "Please check your email and verify your email",
            });
        }
    });
});

router.post('/verifymail', auth.optional, async (req, res, next) => {
    let email_otp = { emailstatus: true }
    Users.findOne({ _id: req.body.id }).then(user => {
        if (!user) {
            return res.json({
                status: "error",
                message: "User not found",
            });
        }

        let otp = user.emailotp;
        console.log(req.body.user_code, otp, user)
        if (req.body.user_code != otp) {
            return res.json({
                status: "error",
                message: "Invalid verification link",
            })
        }

        return Users.findByIdAndUpdate(
            { _id: req.body.id },
            { $set: email_otp },
            { new: true }
        );
    }).then(changed => {
        return res.json({
            status: 200,
            message: "Your account has been verified successfully",
            data: changed
        })
    }).catch(err => {
        return res.json({
            status: "error",
            message: err,
        })
    });
});


module.exports = router;