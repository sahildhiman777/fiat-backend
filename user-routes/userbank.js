const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Users = mongoose.model('Users');
const UserBanks = mongoose.model('Userbank');
const BankList = mongoose.model('BankList');
const Currency = mongoose.model('Currency');
const config = require('../config/config');
const CryptoJS = require("crypto-js");


// Add Bank
router.post('/add_Bank', auth.required, async (req, res, next) => {
    let bank_data = {
        user_id: req.body.user_id,
        bank_country_name: req.body.country_name,
        user_bank_name: req.body.bank_name,
        user_bank_account_number: req.body.account_number,
        user_bank_acount_holder_name: req.body.account_holder_name,
        currency: req.body.currency,
    }
    Users.findOne({ _id: req.body.user_id }).then(user => {
        if (user) {
            UserBanks.findOne({ user_id: req.body.user_id, user_bank_name: req.body.bank_name, deleted: 0 }).then(bank => {
                if (bank == null) {
                    let savedBank = new UserBanks(bank_data);
                    savedBank.save()
                    return res.json({
                        code: 200,
                        status: true,
                        message: 'Bank details has been added',
                        data: savedBank
                    })
                }
                else {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'Bank or Account is already exist',
                        data: []
                    })
                }
            }).catch(err => {
                return res.json({
                    code: 200,
                    status: false,
                    message: 'bank is not added',
                })
            });

        } else {
            return res.json({
                code: 500,
                status: false,
                message: 'User is not Found',
                data: []
            })
        }
    }).catch(err => {
        return res.json({
            code: 500,
            status: false,
            message: 'bank is not added',
        })
    });
});




router.get('/bank_list', auth.required, auth.isUser, async (req, res, next) => {
    try {
        let userId = req.body.loggedInUserData._id;
        Users.findOne({ _id: userId }).then(user => {
            if (user != null) {
                let allBank = UserBanks.find({ user_id: userId, deleted: 0 }).then(banklist => {
                    if (banklist != null) {
                        return res.json({
                            code: 200,
                            status: true,
                            message: 'All Bank fetch sucessfully',
                            data: banklist
                        })
                    } else {
                        return res.json({
                            code: 200,
                            status: false,
                            message: 'No bank list is Found',
                            data: []
                        })
                    }
                }).catch(err => {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'bank is not found',
                    })
                })
            }
        }).catch(err => {
            return res.json({
                code: 200,
                status: false,
                message: 'bank list is not found',
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime',
            data: []
        })
    }

});

router.put('/editbank', async (req, res, next) => {
    try {
        let userId = req.body.user_id;
        let bankId = req.body.bankId;
        let updatedRecord = {
            bank_country_name: req.body.country_name,
            user_bank_name: req.body.bank_name,
            user_bank_account_number: req.body.account_number,
            user_bank_acount_holder_name: req.body.account_holder_name,
        }
        Users.findOne({ _id: req.body.user_id }).then(user => {
            if (user != null) {
                UserBanks.findOneAndUpdate(
                    { _id: bankId, user_id: userId },
                    { $set: updatedRecord },
                    { new: true }
                ).then(banklist => {
                    if (banklist != null) {
                        return res.json({
                            code: 200,
                            status: true,
                            message: 'Bank details has been updated',
                            data: banklist
                        })
                    } else {
                        return res.json({
                            code: 200,
                            status: false,
                            message: 'No bank Account is Found',
                            data: []
                        })
                    }
                }).catch(err => {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'bank is not found',
                    })
                })
            }
        }).catch(err => {
            return res.json({
                code: 200,
                status: false,
                message: 'bank list is not found',
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime,klklkllk',
            data: []
        })
    }

});

// delete banks
router.post('/deletebank/:userid', async (req, res, next) => {
    try {
        let userId = req.params.userid;
        let deleteId = req.body.bankId
        // let accountNumber = req.body.data.user_bank_account_number;
        let updatedStatus = {
            activeStatus: false,
            deleted: 1
        }
        Users.findOne({ _id: req.params.userid }).then(user => {
            if (user != null) {
                UserBanks.findOneAndUpdate({ _id: deleteId }, { $set: updatedStatus }).then(banklist => {
                    if (banklist != null) {
                        return res.json({
                            code: 200,
                            status: true,
                            message: 'delete bank detail',
                            data: []
                        })
                    } else {
                        return res.json({
                            code: 200,
                            status: false,
                            message: 'No bank list is Found',
                            data: []
                        })
                    }
                }).catch(err => {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'bank is not found',
                    })
                })
            }
        }).catch(err => {
            return res.json({
                code: 200,
                status: false,
                message: 'bank list is not found',
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime',
            data: []
        })
    }
});

router.post('/bank/list', async (req, res, next) => {
    const { country } = req.body;
    let query = {};
    if (country && country != '') {
        const bytes = CryptoJS.AES.decrypt(country, config.secretAes);
        const plaintext = bytes.toString(CryptoJS.enc.Utf8);
        if (plaintext != "")
            query['country'] = plaintext.toUpperCase()
    }

    return BankList.find(query).then(dbBanks => {
        let banks = dbBanks.map(b => b.toObject());

        const str = JSON.stringify(banks);
        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
        return res.status(200).json({
            status: true,
            data: ciphertext
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            error: (CryptoJS.AES.encrypt(err.toString(), config.secretAes)).toString()
        });
    });
});

router.get('/cuntry/list', async (req, res) => {
    const currency = await Currency.aggregate([
        { $group: { _id: "$country", currency: { $push: "$currency_code" } } }]).then(c => c);

    let countries = currency.map(function (c) {
        let d = [];
        d.push(c.currency[0]);
        d.push(c._id);
        return d;
    });
    const str = JSON.stringify(countries);

    const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
    return res.status(200).json({
        status: true,
        data: ciphertext
    });
});

router.get('/view_Bank', auth.required, async (req, res, next) => {
    try {
        let bank_id = req.query.bankid;
        Users.findOne({ _id: req.body.user_id }).then(user => {
            if (user != null) {
                UserBanks.findOne({ _id: bank_id, deleted: 0 }).then(banklist => {
                    if (banklist != null) {
                        return res.json({
                            code: 200,
                            status: true,
                            message: 'All Bank fetch sucessfully',
                            data: banklist
                        })
                    }
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'No bank Found',
                        data: []
                    })
                }).catch(err => {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'bank is not found',
                    })
                })
            }
        }).catch(err => {
            return res.json({
                code: 200,
                status: false,
                message: 'bank list is not found',
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime',
            data: []
        })
    }
});

// update BANK
router.put('/currencyList', auth.required, async (req, res, next) => {
    try {
        Currency.find().then(currencyList => {
            res.json({
                status: 200,
                message: 'sucessfull',
                data: currencyList
            })
        }).catch((err) => {
            res.json({
                status: false,
                message: 'err'
            })
        })
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime',
            data: []
        })
    }
});

module.exports = router;