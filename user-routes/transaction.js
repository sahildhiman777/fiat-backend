const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('./auth');
const Transaction = mongoose.model('Transaction');
const Userwallets = mongoose.model('Userwallets');
const Customer = mongoose.model('Customer');
const Users = mongoose.model('Users');
const CryptoJS = require("crypto-js");
const config = require('../config/config');
const moment = require('moment');
const helper = require('./helper');
const Currency = mongoose.model('Currency');
const Settings = mongoose.model('Settings');
const io = require('socket.io')();
const QRCode = require('qrcode');
const request = require("request");
const rateLimit = require("express-rate-limit");
const WAValidator = require('wallet-address-validator');
const SonicxWeb = require('sonicxweb');
const sonicxWeb = new SonicxWeb({
    fullNode: 'https://fullnode.sonicxhub.com',
    solidityNode: 'https://solnode.sonicxhub.com',
    eventServer: 'https://event.sonicxhub.com/'
});

const apiLimiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 5,
    message: "Too many requests from this IP, please try again after sometime",
    skipFailedRequests: true
});
const PayusAPI = require('@payus/payus-sdk')
const fetch = require('node-fetch');
//const Settings = mongoose.model('Settings');

const notify_exchange = (transaction) => {
    const notify_url = transaction.notify_url;

    // delete transaction['_id'];
    delete transaction['deposit'];
    delete transaction['edited'];
    delete transaction['updated_at'];
    delete transaction['notify_url'];
    delete transaction['return_url'];
    delete transaction['edited_by'];
    delete transaction['edited_at'];
    delete transaction['notifyHold'];
    delete transaction['__v'];
    const str = JSON.stringify(transaction);
    const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
    var options = {
        method: 'POST',
        url: notify_url,
        headers: { 'content-type': 'application/json' },
        body: {
            data: ciphertext
        },
        json: true
    };
    request(options, function (error, response, body) {
    });
}

const socketUpdate = () => {

    var options = {
        method: 'POST',
        url: config.socket_update,
        headers: { 'content-type': 'application/json' },
        body: {
            data: []
        },
        json: true
    };
    request(options, function (error, response, body) {
    });
}



router.post('/test-payment', async (req, res, next) => {
    // let order = req.body.order;

    // const bytes = CryptoJS.AES.decrypt(order, config.secret);
    // const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    // if (!plaintext) {
    //     res.json({ status: false, data: 'data not found' });
    //     return;
    // }

    // object = JSON.parse(plaintext);
    // console.log(object)
    // if (!object) {
    //     res.json({ status: false, data: 'data not found' });
    //     return;
    // }

    const {
        coin_name,
        coin_amount,
        total,
        currency_code,
        email,
        phone,
        user_id,
        platform,
        notify_url,
        return_url,
        transaction_id,
        coin_address
    } = req.body;

    let redirectUrl = return_url ? return_url : config.server1;

    if (!coin_name || !coin_amount || !total || !email || !currency_code || !user_id || !platform) {
        const data = {
            status: false,
            data: 'All Fields are required',
            code: 2001,
            fields: {
                coin_name: 'required',
                coin_amount: 'required',
                total: 'required',
                currency_code: 'required',
                email: 'required',
                phone: 'optional',
                user_id: 'required',
                platform: 'required',

            }
        };
        const str = JSON.stringify(data);
        const ciphertext = CryptoJS.AES.encrypt(str, config.secret);

        res.status(301).redirect(`${redirectUrl}?t=${encodeURI(ciphertext)}`);
    } else {
        const order = {
            coin_name,
            coin_amount,
            currency_code,
            total,
            email,
            phone,
            user_id,
            platform,
            notify_url,
            return_url,
            transaction_id,
            coin_address,
            code: Math.floor(100000 + Math.random() * 900000)

        }
        const str = JSON.stringify(order);
        const ciphertext = (CryptoJS.AES.encrypt(str, config.secret)).toString();
        const data = {
            user: ciphertext,
            expire: (new Date()).getTime() + (300)
        }
        req.body.order = JSON.stringify(data);
        const url = `${config.server1}banks/active?code=${currency_code}&type=payin`;
        const banksNotFound = {
            status: false,
            data: 'Cannot load banks',
            code: 2002,
        };
        const strbanksNotFound = JSON.stringify(banksNotFound);
        const cipherBankNotFound = (CryptoJS.AES.encrypt(strbanksNotFound, config.secret)).toString();
        fetch(url)
            .then(res => res.json())
            .then(json => {
                if (json.status) {
                    if (json.data.length == 0) {
                        const banksnotavaialble = {
                            status: false,
                            data: 'bank not available with selected currency',
                            code: 2002,
                        };
                        const strbanksnotavaialble = JSON.stringify(banksnotavaialble);
                        const cipherBankNotav = (CryptoJS.AES.encrypt(strbanksnotavaialble, config.secret)).toString();

                        res.status(301).redirect(`${redirectUrl}?t=${encodeURI(cipherBankNotav)}`);
                    } else {
                        res.render('payment', { data: order, banks: json.data, app_url: config.app_url });
                        return;
                    }
                } else {
                    console.log('Error', json.status);
                    res.status(301).redirect(`${redirectUrl}?t=${encodeURI(cipherBankNotFound)}`);
                }
            })
            .catch(error => {
                res.status(301).redirect(`${redirectUrl}?t=${encodeURI(cipherBankNotFound)}`);
            });
        // res.status(301).redirect(`${redirectUrl}?t=${ciphertext2}`);

    }

});

router.post('/buycoin', auth.required, auth.isUser, async (req, res, next) => {
    try {
        let loggedUser = await Users.findOne({
            _id: req.body.loggedInUserData._id
        }).catch(err => {
            console.log(err);
            loggedUser = false;
        });

        if (!loggedUser || !loggedUser.emailstatus) {
            return res.json({
                status: 401,
                message: "Verify your email first.",
                data: []
            });
        }

        if (
            loggedUser.kyc_level1 != "approved"
        ) {
            return res.json({
                status: 401,
                message: "Please complete your KYC first",
                data: []
            });
        }

        let address_valid = true;
        if (req.body.coin_name != "SOX" && req.body.coin_name != "$USD") {
            address_valid = WAValidator.validate(req.body.coin_address, req.body.coin_name);
            console.log("address_valid", address_valid)
        } else {
            address_valid = sonicxWeb.isAddress(req.body.coin_address)
        }

        if (!address_valid) {
            return res.json({
                status: 401,
                message: "Coin address is invalid",
                data: []
            });
        }

        let tran_data = {
            total: req.body.total,
            coin_amount: req.body.coin_amount,
            coin_name: req.body.coin_name,
            coin_address: req.body.coin_address,
            currency_code: req.body.currency_code,
            user_id: req.body.user_id,
            email: req.body.email,
            transaction_id: req.body.transaction_id,
            notify_url: req.body.notify_url,
            platform: req.body.platform,
            phone: req.body.phone,
            return_url: req.body.return_url,
            currency: req.body.currency,
            code: req.body.code,
            deposit: req.body.deposit,
            status: req.body.status,
            bank: req.body.bank,
            // created_at : req.body.created_at,
            // updated_at: req.body.updated_at,
            edited: req.body.edited,
            payus_status: req.body.payus_status,

        };
        let savedtransaction = new Transaction(tran_data);
        savedtransaction.save().then(savedata => {
            return res.json({
                status: 200,
                message: "successfully",
                data: savedata
            });
        }).catch(err => {
            return res.json({
                status: "error",
                message: err,
            });
        })
    } catch (err) {
        console.error(err);
        return res.status(500).json({
            code: 500,
            message: 'Something went wrong,Try after sometime',
            data: []
        })
    }

})

router.post('/', async (req, res, next) => {
    const { body: {
        order, bank
    }
    } = req;
    // console.log('order.user,', req.body)

    const bytes = CryptoJS.AES.decrypt(order.user, config.secretAes);
    const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    const object = JSON.parse(plaintext);
    // console.log(object)
    // console.log('bank', bank)
    if (!object) {
        const data = {
            status: false,
            data: 'Order Not found',
        };
        const str = JSON.stringify(data);
        const ciphertext = CryptoJS.AES.encrypt(str, config.secretAes);
        res.json({ status: false, data: ciphertext });
        return;
    }

    const currency = await Currency.find({ currency_code: object.currency_code }).select('_id').then(d => d).catch(e => { return false })
    let CurrId = '';
    if (currency.length > 0) {
        CurrId = currency[0]._id;
    }
    //const DummyDate = moment().subtract(400, 'days');
    const neworder = object;
    neworder.bank = bank;
    // neworder.created_at = DummyDate;
    // neworder.updated_at = new Date();
    // neworder.status = 'approved';
    neworder.created_at = new Date();
    neworder.updated_at = new Date();
    neworder.status = 'pending';
    neworder.payus_status = 'pending';
    neworder.currency = CurrId;
    neworder.deposit = true;
    neworder.edited = false;

    return Transaction.init().then(async () => {
        const curr = new Transaction(neworder);
        await curr.populate('bank', '_id bank_name').execPopulate();
        await curr.populate('currency', '_id currency currency_code').execPopulate();
        return curr.save();
    }).then(async (tr) => {
        const customerObj = {};
        customerObj.email = tr.email;
        customerObj.phone = tr.phone;
        const IsExist = await Customer.find(customerObj).then((data) => (data));
        if (IsExist.length === 0) {
            customerObj.status = true;
            customerObj.created_at = new Date();
            customerObj.updated_at = new Date();
            const customer = new Customer(customerObj);
            await customer.save();
        }
        let notifyObj = Object.assign({}, tr.toObject());
        socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
        const str = JSON.stringify(tr);
        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
        notify_exchange(notifyObj);
        res.json({
            status: true,
            data: ciphertext,
        })
    }).catch((err) => {
        const strErr = JSON.stringify(err);
        const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
        res.json({
            status: false,
            data: ciphertextErr,
        })
    });
});
router.get('/', async (req, res, next) => {

    //await function (deposit=false, dashboard =true,time='month')

    const dailyWithdraws = await helper.groupedSum(false, true, 'day');
    const monthlyWithdraws = await helper.groupedSum(false, true, 'month');
    const weeklyWithdraws = await helper.groupedSum(false, true, 'week');
    const hourlyWithdraws = await helper.groupedSum(false, true, 'hour');
    const totalWithdraws = await helper.aggrecate(false, 'all', 'total');
    const dailyDeposit = await helper.groupedSum(true, true, 'day');
    const monthlyDeposit = await helper.groupedSum(true, true, 'month');
    const weeklyDeposit = await helper.groupedSum(true, true, 'week');
    const hourlyDeposit = await helper.groupedSum(true, true, 'hour');
    const totalDeposit = await helper.aggrecate(true, 'all', 'total');

    const deposits = await helper.transactionsSearch(true, start, end, cur, input);
    const withdraws = await helper.transactionsSearch(false, start, end, cur, input);
    return res.status(200).json({
        status: true,
        data: {
            deposits, withdraws,
            dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});
router.get('/qrcode', async (req, res, next) => {
    const d = req.query.code;
    QRCode.toDataURL(d)
        .then(url => {
            var base64Data = url.replace(/^data:image\/png;base64,/, '');
            var img = new Buffer(base64Data, 'base64');

            res.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            res.end(img);
            // console.log(url)
        })
        .catch(err => {
            // res.json({ err });
            console.error(err)
        })


});
router.get('/search', async (req, res, next) => {
    const { body: {
        start, end, currency, input, status, bank, type
    }
    } = req;

    // const deposits = await helper.transactionsSearch(true, start, end, currency, input, status, bank);
    // const withdraws = await helper.transactionsSearch(false, start, end, currency, input, status, bank);
    const searchData = await helper.transactionsSearch('sell', start, end, 'VND', input, status, bank);

    return res.status(200).json({
        status: true,
        data: {
            searchData: searchData.tr, searchDataGrouped: searchData.grouped
            // deposits: deposits.tr, withdraws: withdraws.tr, depositsgrouped: deposits.grouped, withdrawsgrouped: withdraws.grouped,
            // dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            // dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});
router.post('/search', auth.required, async (req, res, next) => {
    const { body: {
        start, end, currency, input, status, bank, type, time, timezone, loggedInUserData
    }
    } = req;

    const searchData = await helper.transactionsSearch(type, start, end, currency, input, status, bank, time, timezone, loggedInUserData);

    return res.status(200).json({
        status: true,
        data: {
            searchData: searchData.tr, searchDataGrouped: searchData.grouped
        },
    });
});

router.post('/list', auth.required, auth.isUser, async (req, res, next) => {
    const {
        type,
        status,
        platform,
        currency_code,
        searchText,
        pageNumber
    } = req.body

    const data = await helper.transactions(type, status, platform, currency_code, searchText, pageNumber);
    data.data = data.data.filter(tran => tran.user_id == req.body.loggedInUserData._id);
    data.total = data.data.length;

    return res.status(200).json({
        status: true,
        data: data
    });

});
router.post('/dashboard', async (req, res, next) => {
    const {
        currency_code
    } = req.body
    //await function (deposit=false, dashboard =true,time='month')
    const dailyWithdraws = await helper.groupedSum(false, true, 'day', currency_code);
    const monthlyWithdraws = await helper.groupedSum(false, true, 'month', currency_code);
    const weeklyWithdraws = await helper.groupedSum(false, true, 'week', currency_code);
    const hourlyWithdraws = await helper.groupedSum(false, true, 'hour', currency_code);
    const totalWithdraws = await helper.aggrecate(false, 'all', 'total', currency_code);
    const dailyDeposit = await helper.groupedSum(true, true, 'day', currency_code);
    const monthlyDeposit = await helper.groupedSum(true, true, 'month', currency_code);
    const weeklyDeposit = await helper.groupedSum(true, true, 'week', currency_code);
    const hourlyDeposit = await helper.groupedSum(true, true, 'hour', currency_code);
    const totalDeposit = await helper.aggrecate(true, 'all', 'total', currency_code);
    return res.status(200).json({
        status: true,
        data: {
            dailyWithdraws, weeklyWithdraws, hourlyWithdraws, monthlyWithdraws, totalWithdraws,
            dailyDeposit, weeklyDeposit, hourlyDeposit, monthlyDeposit, totalDeposit, totalWithdraws,
        },
    });

});

router.get('/single/:cId', auth.required, async (req, res, next) => {
    const { payload: { id } } = req;
    const cId = req.params.cId;

    Transaction.findById(cId).populate('bank').then(d => {
        return res.status(200).json({
            status: true,
            data: d,
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});
router.post('/withdraws/search', auth.required, async (req, res, next) => {
    const { body: {
        search
    }
    } = req;
    if (!search) {
        return res.status(200).json({
            status: false,
            data: 'search parameter required',
        });
    } else if (!search.name) {
        return res.status(200).json({
            status: false,
            data: 'search parameter column required',
        });
    } else if (search.name == 'date') {
        const tonight = moment('search').endOf('day').toString();
        // db.users.find({ "name": /m/ })

        Transaction.find({
            created_at: {
                $gte: new Date(start),
                $lte: new Date(tonight)
            },
            deposit: false,
        }).populate('bank').then(d => {
            return res.status(200).json({
                status: true,
                data: d,
            });
        }).catch(err => {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        });
    } else {
        //db.users.find({"name": /m/})
        Transaction.find(search).populate('bank').then(d => {
            return res.status(200).json({
                status: true,
                data: d,
            });
        }).catch(err => {
            return res.status(200).json({
                status: false,
                data: err.toString(),
            });
        });
    }

});
router.get('/report', async (req, res, next) => {
    //await helper.aggrecate(deposit or withdraws, report time, either total money or coin_amount,)
    // const dailyWithdraws = await helper.aggrecate(false, 'daily', 'total');
    // const weeklyWithdraws = await helper.aggrecate(false, 'week', 'total');
    // const monthlyWithdraws = await helper.aggrecate(false, 'month', 'total');
    // const totalWithdrws = await helper.aggrecate(false, 'all', 'total');
    // const dailyDeposit = await helper.aggrecate(true, 'daily', 'total');
    // const weeklyDeposit = await helper.aggrecate(true, 'week', 'total');
    // const monthlyDeposit = await helper.aggrecate(true, 'month', 'total');
    // const totalDeposit = await helper.aggrecate(true, 'all', 'total');
    const deposits = await helper.transactions(true, false /*'pending'*/);
    const withdraws = await helper.transactions(false, false /*'pending'*/);
    return res.status(200).json({
        status: true,
        data: {
            deposits, withdraws, dailyWithdraws, weeklyWithdraws, monthlyWithdraws, totalWithdrws,
            dailyDeposit, weeklyDeposit, monthlyDeposit, totalDeposit
        },
    });
});

router.put('/:cId', auth.required, async (req, res, next) => {
    const { body: {
        bank,
        status,
        loggedInUserData
    }
    } = req;

    const cId = req.params.cId;
    const tr = await Transaction.findById(cId).then(d => d);
    if (tr.edited) {
        return res.status(200).json({
            status: false,
            data: 'Cannot edit this transaction.',
        });
    } else {

        const c = {};
        c.status = status == 'approved' ? 'approved' : 'rejected';
        c.edited = true;
        c.updated_at = new Date();
        c.edited_by = loggedInUserData._id;
        c.edited_at = new Date();

        if (tr.deposit === false)
            c.bank = bank;

        const cId = req.params.cId;
        // const notifyData = {
        //     id: req.params.cId,
        //     status,
        // }

        if (c.status === 'approved' && tr.deposit === true) {
            const payus = new PayusAPI(config.payusAccessToken);

            var payusResult = payus.withdraw({
                coin_code: tr.coin_name,
                amount: tr.coin_amount,
                destination_tag: tr.payus_destination_tag,
                withdraw_address: tr.coin_address,
                order_id: tr.transaction_id
            });

            return payusResult.then(result => {
                if (result.status === 'success') {

                    c.payus_transaction_id = result.data.transaction_id;
                    c.transaction_hash = result.data.tx_hash
                    c.payus_status = c.transaction_hash ? 'completed' : 'waiting_for_confirmation';
                    const updateObj = c;
                    Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                        if (model) {
                            // helper.notify(notifyData);
                            Transaction.findById(cId).populate('bank').populate('currency').then(d => {

                                let notifyObj = Object.assign({}, d.toObject());
                                notify_exchange(notifyObj);

                                return res.status(200).json({
                                    status: true,
                                    data: d,
                                });
                            }).catch(err => {
                                return res.status(200).json({
                                    status: false,
                                    data: err.toString(),
                                });
                            });
                        }
                        else if (err) {
                            return res.status(200).json({
                                status: false,
                                data: err.toString(),
                            });
                        }

                    });
                } else {
                    return res.json({
                        status: false,
                        data: result.message,
                    })
                }

            }).catch(error => {
                return res.json({
                    status: false,
                    data: error.toString(),
                })
            })
        } else {
            const updateObj = c;
            Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                if (model) {
                    // helper.notify(notifyData);
                    Transaction.findById(cId).populate('bank').populate('currency').then(d => {

                        let notifyObj = Object.assign({}, d.toObject());
                        notify_exchange(notifyObj);

                        return res.status(200).json({
                            status: true,
                            data: d,
                        });
                    }).catch(err => {
                        return res.status(200).json({
                            status: false,
                            data: err.toString(),
                        });
                    });
                }
                else if (err) {
                    return res.status(200).json({
                        status: false,
                        data: err.toString(),
                    });
                }

            });
        }
    }
});

router.delete('/:cId', auth.required, async (req, res, next) => {

    const cId = req.params.cId;
    Transaction.findByIdAndRemove(cId, (error, data) => {
        if (error) {
            return res.status(200).json({
                status: false,
                data: error.toString(),
            });
        } else {
            return res.status(200).json({
                status: true,
                data: [],
            });

        }
    });
});
router.get('/status/:cId', async (req, res, next) => {
    const cId = req.params.cId;

    Transaction.findById(cId).then(d => {
        return res.status(200).json({
            status: true,
            data: d.status
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});
router.post('/withdraw', apiLimiter, auth.required, auth.isUser, async (req, res, next) => {
    let loggedUser = await Users.findOne({
        _id: req.body.loggedInUserData._id
    }).catch(err => {
        loggedUser = false;
    });

    let userWallet = await Userwallets.findOne({
        user_id: req.body.user_id,
        coin_code: req.body.coin_name
    }).catch(err => {
        userWallet = false;
    });
    if (!loggedUser || !loggedUser.emailstatus) {
        return res.json({
            status: 401,
            message: "Verify your email first.",
            data: []
        });
    }

    if (
        loggedUser.kyc_level1 != "approved" ||
        loggedUser.kyc_level2 != "approved" ||
        loggedUser.kyc_level3 != "approved"
    ) {
        return res.json({
            status: 401,
            message: "Please complete your KYC first and wait until its approved.",
            data: []
        });
    }

    if (!userWallet || Number(userWallet.coin_balance) - Number(req.body.coin_amount) < 0) {
        return res.json({
            status: 401,
            message: "You dont have enough coins to sell",
            data: []
        });
    }

    let tran_data = {
        total: req.body.total,
        user_id: req.body.user_id,
        platform: req.body.platform,
        coin_amount: req.body.coin_amount,
        coin_name: req.body.coin_name,
        email: req.body.email,
        phone: req.body.phone,
        user_bank_name: req.body.user_bank_name,
        user_bank_account_number: req.body.user_bank_account_number,
        user_bank_acount_holder_name: req.body.user_bank_acount_holder_name,
        user_currency_code: req.body.user_currency_code,
        status: '',
        notify_url: req.body.notify_url,
        return_url: req.body.return_url,
        deposit: '',
        transaction_id: req.body.transaction_id,
        created_at: '',
        updated_at: '',
        edited: '',

    };
    const {
        coin_name,
        coin_amount,
        email,
        notify_url,
        return_url,
        transaction_id
    } = req.body;

    const neworder = tran_data;
    neworder.created_at = new Date();
    neworder.updated_at = new Date();
    neworder.status = 'pending';
    neworder.edited = false;
    neworder.deposit = false;
    const payus = new PayusAPI(config.payusAccessToken);
    if (coin_name == 'USDT') {
        var payusResult = payus.paymentCryptoRest({
            currency: coin_name,
            return_url: return_url,
            notify_url: notify_url,
            coin_code: 'ERC20-USDT',
            product_amount: coin_amount,
            invoice_id: transaction_id,
            email: email
        });
    } else {
        var payusResult = payus.paymentCryptoRest({
            currency: coin_name,
            return_url: return_url,
            notify_url: notify_url,
            coin_code: coin_name,
            product_amount: coin_amount,
            invoice_id: transaction_id,
            email: email
        });
    }
    return payusResult.then(result => {
        if (result.status === 'success') {
            neworder.payus_transaction_id = result.data.result.transaction_id;
            neworder.payus_amount_orignal = result.data.result.amount;
            neworder.payus_amount_usd = result.data.product_amount;
            neworder.payus_amount_crypto = result.data.result.product_crypto_amount;
            neworder.payus_destination_tag = result.data.result.destination_tag;
            neworder.payus_mining_fees = result.data.result.mining_fees;
            neworder.payus_total_payable_amount = result.data.result.total_payable_amount;
            neworder.payus_fees = result.data.result.payus_fees;
            neworder.payus_total_fees = result.data.result.fees;
            neworder.payus_usd_rate = result.data.result.usd_rate;
            neworder.payus_expire_datetime = result.data.result.expire_datetime;
            neworder.payus_payment_tracking = result.data.result.payment_tracking;
            neworder.coin_address = result.data.result.crypto_address;
            neworder.payus_status = 'pending';
        } else {
            const strErr = JSON.stringify(result);
            // const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: result,
            })
        }
        return Transaction.init().then(async () => {
            const curr = new Transaction(neworder);
            await curr.populate('bank', '_id bank_name').execPopulate();
            await curr.populate('currency', '_id currency currency_code').execPopulate();
            return curr.save();
        }).then(async (tr) => {
            const customerObj = {};
            customerObj.email = tr.email;
            customerObj.phone = tr.phone;
            const IsExist = await Customer.find(customerObj).then((data) => (data));
            if (IsExist.length === 0) {
                customerObj.status = true;
                customerObj.created_at = new Date();
                customerObj.updated_at = new Date();
                const customer = new Customer(customerObj);
                await customer.save();
            }
            socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
            let notifyObj = Object.assign({}, tr.toObject());
            const str = JSON.stringify(tr);
            const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
            notify_exchange(notifyObj);

            if (tr.deposit === false) {
                Userwallets.findOneAndUpdate(
                    {
                        user_id: tr.user_id,
                        coin_code: tr.coin_name
                    },
                    {
                        $inc: { coin_balance: Number(tr.coin_amount) * -1 }
                    })
                    .then(() => console.log("Wallet updated successfully"))
                    .catch(err => console.log("Error updating wallet", err));
            }

            return res.json({
                status: true,
                data: notifyObj,
            });
        }).catch((err) => {
            const strErr = JSON.stringify(err);
            const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: err,
            })
        });
    }).catch(error => {
        const strErr = JSON.stringify(error);
        const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
        return res.json({
            status: false,
            data: error,
        })
    })
});

router.post('/withdraw/:cId', apiLimiter, async (req, res, next) => {
    const cId = req.params.cId;
    let validationError = false;

    let order = req.body.order;

    const bytes = CryptoJS.AES.decrypt(order, config.secretAes);
    const plaintext = bytes.toString(CryptoJS.enc.Utf8);
    if (!plaintext) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    object = JSON.parse(plaintext);
    // const object = (order);
    if (!object) {
        res.json({ status: false, data: 'data not found' });
        return;
    }

    const fields = ["transaction_hash", "coin_address"];
    fields.forEach((field) => {
        if (!object[`${field}`]) {
            validationError = true;
            res.json({ status: false, data: `${field} is required` });
            return;
        }
    });

    if (validationError) {
        return;
    }

    Transaction.findById(cId).then(tr => {

        const payus = new PayusAPI(config.payusAccessToken);

        var payusResult = payus.getBalance({
            transaction_id: tr.payus_transaction_id
        });

        return payusResult.then(result => {
            const c = {};
            c.updated_at = new Date();
            c.transaction_hash = object['transaction_hash'];
            c.coin_address = object['coin_address'];
            c.payus_status = tr.payus_status == 'pending' ? 'waiting_for_confirmation' : tr.payus_status;
            c.actual_deposit_amount = typeof result.data == 'object' ? 0 : parseFloat(result.data).toFixed(8);

            const updateObj = c;

            Transaction.findByIdAndUpdate(cId, updateObj, { new: true }, function (err, model) {
                if (model) {
                    Transaction.findById(cId).populate('bank').populate('currency').then(d => {
                        socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
                        let notifyObj = Object.assign({}, d.toObject());
                        const str = JSON.stringify(d);
                        const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
                        notify_exchange(notifyObj);
                        return res.json({
                            status: true,
                            data: ciphertext,
                        });
                    }).catch(err => {
                        return res.status(200).json({
                            status: false,
                            data: err.toString(),
                        });
                    });
                }
                else if (err) {
                    return res.status(200).json({
                        status: false,
                        data: err.toString(),
                    });
                }
            });
        }).catch(error => {
            const strErr = JSON.stringify(error);
            const ciphertextErr = (CryptoJS.AES.encrypt(strErr, config.secretAes)).toString();
            return res.json({
                status: false,
                data: ciphertextErr,
            })
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

router.post('/notify_me', async (req, res, next) => {
    const {
        body: {
            invoice_id,
            transaction_id,
            txid,
            coin_code,
            product_amount_crypto,
            product_amount_base,
            crypto_address,
            org_amount,
            payus_fees,
            mining_fees,
            status
        }
    } = req;

    Transaction.findOne({
        payus_transaction_id: transaction_id,
    }).then(tr => {
        let notifyObj = Object.assign({}, tr.toObject());

        notifyObj.payus_status = status;

        // if(tr.actual_deposit_amount){
        //     notify_exchange(notifyObj);
        //     return res.status(200).json({
        //         status: true,
        //         txid: tr.transaction_hash,
        //     });
        // }
        const c = {};
        c.updated_at = new Date();
        c.payus_status = status;
        if (status == 'completed' || status == 'waiting_for_confirmation') {
            c.actual_deposit_amount = parseFloat(product_amount_crypto).toFixed(8);
            notifyObj.actual_deposit_amount = c.actual_deposit_amount;
        }

        if (status === 'expired' && tr.status === 'pending')
            c.status = 'rejected';

        if (!tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation'))
            c.notifyHold = true;

        if (tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation'))
            c.notifyHold = false;

        const updateObj = c;

        Transaction.findByIdAndUpdate(tr._id, updateObj, { new: true }, function (err, model) {
            if (model) {
                socketUpdate(); // req.io.sockets.emit('update', (new Date()).getTime());
                if (status !== 'pending')
                    notifyObj.status = status;

                if (status === 'expired' && tr.status === 'pending')
                    notifyObj.status = 'rejected';

                if (status === 'pending' || (tr.transaction_hash && (status === 'completed' || status === 'waiting_for_confirmation')) || (status === 'expired' && tr.status === 'pending'))
                    notify_exchange(notifyObj);

                return res.status(200).json({
                    status: true,
                    txid: txid,
                });
            }
            else if (err) {
                return res.status(200).json({
                    status: false,
                    data: err.toString(),
                });
            }
        });
    }).catch(err => {
        return res.status(200).json({
            status: false,
            data: err.toString(),
        });
    });
});

module.exports = router;