const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('./auth');
const Users = mongoose.model('Users');
const Userwallets = mongoose.model('Userwallets');
const otpModel = mongoose.model('Otp');
var speakeasy = require('speakeasy');
var validator = require("email-validator");
var jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mail = require('./mail');
var nodemailer = require('nodemailer');
const config = require('../config/config')
const global_coins = ['BTC', 'ETH', 'USDT'];
const sox_coins = ['SOX', '$USD']
const axios = require('axios');


const getCoinRates = async (coin_list, currency) => {
    let symboldata = [];
    let sox_data, curr_usd_rate, baseSoxPair;
    let external_apis = [
        'http://api.sonicex.com/FrontAPI/api/Transaction/GetTradePairAsset',
        `https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&ids=tether`
    ];

    try {
        let external_api_ary = external_apis.map(url => {
            return new Promise(async (resolve, reject) => {
                axios.get(url).then(res => {
                    return resolve(res.data);
                }).catch(err => {
                    console.log('Error fetching data', err);
                    return resolve(false);
                });
            })
        });

        return Promise.all(external_api_ary).then(data => {
            sox_data = data[0];
            curr_usd_rate = data[1][0] && data[1][0].current_price;
            baseSoxPair = sox_data.response && sox_data.response.find(basePair => basePair.BaseCurrencyName == 'USDX')

            let promiseArray = coin_list.map(coinObj => {
                return new Promise(async (resolve, reject) => {
                    let ticker_data = {};
                    ticker_data = {
                        "current_price": 0,
                        "high_24h": 0,
                        "low_24h": 0
                    }

                    if (global_coins.some(coin => coin == coinObj.coin_code)) {
                        let api_res = await axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&ids=${coinObj.coin_name}`).catch(err => {
                            console.log(`Error gettng price for ${currency}/${coinObj.coin_code}`, err);
                        });
                        api_res = api_res.data;

                        if (api_res.length > 0) {
                            ticker_data = api_res[0];
                        }
                        
                        coinObj.currency_rate = ticker_data.current_price
                        return resolve(coinObj);
                    } else if (sox_coins.some(coin => coin == coinObj.coin_code)) {
                        let usd_rate = 1;
                        ticker_data = {
                            "CurrentRate": 0,
                            "Low52Week": 0,
                            "HighWeek": 0
                        }

                        if (curr_usd_rate > 0) {
                            usd_rate = curr_usd_rate;
                        }

                        if (coinObj.coin_code == '$USD') {
                            coinObj.currency_rate = usd_rate
                            return resolve(coinObj);
                        } else {
                            let api_res = baseSoxPair && baseSoxPair.PairList.find(pair => pair.PairName == `${coinObj.coin_code}_USDX`);
                            if (api_res) {
                                ticker_data = api_res;
                            }
                            coinObj.currency_rate = (Number(ticker_data.CurrentRate) * usd_rate).toFixed(6)
                            return resolve(coinObj);
                        }
                    } else {
                        return resolve(false);
                    }
                });
            });

            return Promise.all(promiseArray)
        }).then(data => {
            symboldata = data.filter(d => d !== false);
            return Promise.resolve(symboldata)
        }).catch(error => {
            console.log(error);
            return Promise.reject(error)
        })
    } catch (error) {
        return Promise.reject(error)
    }
}
router.get('/wallets_list', auth.required, auth.isUser, async (req, res, next) => {
    try {
        let userId = req.body.loggedInUserData._id;
        const currency = req.query.currency || false;

        Users.findOne({ _id: userId }).then(user => {
            if (user != null) {
                Userwallets.find({ user_id: userId }).then(async walletsList => {
                    if (walletsList != null) {
                        let walletData;
                        if (currency) {
                            walletData = await getCoinRates(walletsList, currency).catch(console.log)
                        }

                        return res.json({
                            code: 200,
                            status: true,
                            message: 'All Wallets fetch sucessfully',
                            data: walletData || walletsList
                        })
                    } else {
                        return res.json({
                            code: 200,
                            status: false,
                            message: 'No Wallets list is Found',
                            data: []
                        })
                    }
                }).catch(err => {
                    return res.json({
                        code: 200,
                        status: false,
                        message: 'Wallets is not found',
                    })
                })
            }
        }).catch(err => {
            return res.json({
                code: 200,
                status: false,
                message: 'Wallets list is not found',
            })
        });
    } catch (err) {
        return res.status(500).json({
            code: 500,
            status: false,
            message: 'Something went wrong ,try after sometime',
            data: []
        })
    }

});

module.exports = router;