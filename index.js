const express = require('express');
//const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
var cookieParser = require('cookie-parser')
var csrf = require('csurf')

var fs = require('fs');
var http = require('http');
var https = require('https');

//const formidable = require('express-formidable');
//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

// var privateKey = fs.readFileSync('/etc/ssl/gdc/giaodichcoin.key', 'utf8');
// var certificate = fs.readFileSync('/etc/ssl/gdc/1c97c34601dfda14.crt', 'utf8');
// var caBundle = fs.readFileSync('/etc/ssl/gdc/gd_bundle-g2-g1.crt', 'utf8');
// var credentials = { key: privateKey, cert: certificate, ca: caBundle };

//Initiate our app
const app = express();
const port = 7000;
const port1 = 7001;

var server = http.createServer(app);
// var server = https.createServer(credentials, app);

var server1 = http.createServer(app);
// var server1 = https.createServer(credentials, app);

var io = require('socket.io')(server1);
io.sockets.on('connection', function (socket) {
  socket.on('echo', function (data) {
    io.sockets.emit('message', data);
  });
});
app.use(function (req, res, next) {
  req.io = io;
  next();
});
//Configure our app
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(cookieParser());
app.use(cors());
app.options('*', cors());
app.options('*', function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader("Access-Control-Allow-Headers", "*");
  res.end();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.use(formidable());
//Configure Mongoose
mongoose.connect('mongodb://127.0.0.1:27017/coindashboard');
//mongoose.connect('mongodb://157.245.186.100:27017/coindashboard');
mongoose.set('debug', true);

//Models & routes
require('./models/Users');
require('./models/Otp');
require('./models/Banks');
require('./models/Banklist');
require('./models/Currency');
require('./models/Customer');
require('./models/UserBank');
require('./models/userWallets');
require('./models/userKyc');
require('./models/Transaction');
require('./models/Settings');
require('./models/Coins');
require('./config/passport');
// const g2fa = require('./routes/g2fa');
// app.use(g2fa.checkUser);
app.use(require('./routes'));
app.use(require('./user-routes'));
app.get('/payment', function (req, res) {
  res.render('payment');
});
app.post('/socket-update', function (req, res) {
  req.io.sockets.emit('update', (new Date()).getTime());
  return res.json({
    status: true,
    data: [],
  });
});
app.use('/*', function (req, res) {
  res.status(500).json({ status: false, msg: 'No route found ' });
});

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    return res.status(401).json({
      status: false,
      data: 'Not authorized',
    });
  } else {
    return res.status(500).json({
      status: false,
      data: 'Something went wrong',
    });
  }
});

server.listen(port, function () {
  console.log(`Server running on http://localhost:${port}/`);
});

server1.listen(port1, function () {
  console.log(`Server running on http://localhost:${port1}/`);
});