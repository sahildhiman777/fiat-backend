const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserBankSchema = new Schema({
    
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    bank_country_name: String,
    user_bank_name: String,
    user_bank_account_number: String,
    user_bank_acount_holder_name: String,
    currency: String,
    activeStatus: {
        type: Boolean,
        default: true
    },
    deleted: {
        type:Number,
        default:0
    },
}, { timestamps: true });
mongoose.model('Userbank', UserBankSchema);