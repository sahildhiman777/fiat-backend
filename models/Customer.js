const mongoose = require('mongoose');

const { Schema } = mongoose;

const CustomerSchema = new Schema({
    name: String,
    email: String,
    dob: Date,
    phone: String,
    status: Boolean,
    created_at: Date,
    updated_at: Date,
});
mongoose.model('Customer', CustomerSchema);