const mongoose = require('mongoose');

const { Schema } = mongoose;

const SettingsSchema = new Schema({
    totalWithdrawsMin:Number,
    totalWithdrawsMax:Number,
    userWithdrawsMin:Number,
    userWithdrawsMax:Number,
    totalDepositsMin:Number,
    totalDepositsMax:Number,
    userDepositsMin:Number,
    userDepositsMax:Number,   
    created_at: Date,
    updated_at: Date,
});
mongoose.model('Settings', SettingsSchema);
