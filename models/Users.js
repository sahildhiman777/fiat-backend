const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require('../config/config')

const { Schema } = mongoose;

const UsersSchema = new Schema({
    email: String,
    google2fa: Boolean,
    email_2fa: Boolean,
    phoneverify_status: Boolean,
    email_2facode: Number,
    mobile_Otp: Number,
    emailotp: String,
    emailstatus: Boolean,
    phone: {
        type: String,
        required: false,
    },
    kyc_level1: {
        type: String,
        enum: ['approved', 'rejected', 'pending', "notuploaded"],
        required: true,
        default: "notuploaded",

    },
    kyc_level2: {
        type: String,
        enum: ['approved', 'rejected', 'pending', "notuploaded"],
        required: true,
        default: "notuploaded",

    },
    kyc_level3: {
        type: String,
        enum: ['approved', 'rejected', 'pending', "notuploaded"],
        required: true,
        default: "notuploaded",

    },
    role: String,
    hash: String,
    salt: String,
    name: String,
    firstname: String,
    lastname: String,
    faKey: String,
    otpauth_url: String,
    banks: [{ type: Schema.Types.ObjectId, ref: 'Banks' }]
});

UsersSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersSchema.methods.validatePassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

UsersSchema.methods.generateJWT = function () {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        email: this.email,
        id: this._id,
        name: this.name,
        role: this.role,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, config.secret);
}

UsersSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        email: this.email,
        name: this.name,
        firstname: this.firstname,
        lastname: this.lastname,
        role: this.role,
        token: this.generateJWT(),
        google2fa: this.google2fa,
        email_2fa: this.email_2fa,
        emailstatus:this.emailstatus,
        phoneverify_status:this.phoneverify_status,
        kyc_level1:this.kyc_level1,
        kyc_level2:this.kyc_level2,
        kyc_level3:this.kyc_level3,
    };
};

mongoose.model('Users', UsersSchema);