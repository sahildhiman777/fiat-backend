const mongoose = require('mongoose');

const { Schema } = mongoose;

const userKycSchema = new Schema(
  {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    level: {type: String, required: true, },
    status: {type: String, required: true, },
    kyc_doc: {type: String, required: true, },	
    created_date: {type: Date},
    modified_date: {type: Date},
});

mongoose.model('userKyc', userKycSchema);