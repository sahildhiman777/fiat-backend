const mongoose = require('mongoose');

const { Schema } = mongoose;

const CurrencySchema = new Schema({
    country: String,
    currency: String,
    currency_code: String,
    logo: String
});
mongoose.model('Currency', CurrencySchema);