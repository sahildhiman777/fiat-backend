const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserWalletsSchema = new Schema(
  {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true,
    },
    coin_name: {type: String, required: true, },
    coin_code: {type: String, required: true, },
    coin_balance: {type: Number, default: 0},
    converted_balance: {type: Number, default: 0},
    currency_rate: {type: Number, default: 0},
    status: {type: Boolean},
    sell_status:{type: Boolean},
    buy_status:{type: Boolean},
    icon:{type: String, default: ""},
    coin_address: {type: String, default: ""},
    destination_tag: {type: String, default: ""},
    crypto_address_json_resp:{type: String, default: ""},	
    created_date: {type: Date},
    modified_date: {type: Date},
});

mongoose.model('Userwallets', UserWalletsSchema);