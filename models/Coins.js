const mongoose = require('mongoose');

const { Schema } = mongoose;

const CurrencySchema = new Schema({
    name: { type: String, required: true },
    symbol: { type: String, required: true },
    icon: { type: String },
    buy_status: { type: Boolean, default: true },
    sell_status: { type: Boolean, default: true },
    status: { type: Boolean, default: true },
});
mongoose.model('Coins', CurrencySchema);