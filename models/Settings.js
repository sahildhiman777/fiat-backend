const mongoose = require('mongoose');

const { Schema } = mongoose;

const SettingsSchema = new Schema({
    totalWithdraws:Number,
    totalPaid:Number,
    totalApproved: Number,
    currency_code: String,
    user_id: { type: Schema.Types.ObjectId, ref: 'Users' },
    created_at: Date,
    updated_at: Date,
    status: String
});
mongoose.model('Settings', SettingsSchema);
