const mongoose = require('mongoose');

const { Schema } = mongoose;

const TransactionSchema = new Schema({
    transaction_id: {
        type: String,
        required: true,
        unique: true
    },
    total: Number,
    coin_amount: Number,
    actual_deposit_amount: Number,
    coin_name: String,
    coin_address: String,
    transaction_hash: String,
    currency_code: String,
    user_id: String,
    platform: String,
    email: String,
    dob: String,
    phone: String,
    bank: { type: Schema.Types.ObjectId, ref: 'Banks' },
    currency: { type: Schema.Types.ObjectId, ref: 'Currency' },
    status: String,
    code: String,
    deposit: Boolean,
    user_bank_name: String,
    user_bank_iban: String,
    user_bank_code: String,
    user_bank_account_number: String,
    user_bank_acount_holder_name: String,
    user_currency: String,
    user_currency_code: String,
    user_currency_name: String,
    notify_url: {
        type: String,
        required: true
    },
    return_url: {
        type: String
    },
    // created_at: Date,
    // updated_at: Date,
    edited: Boolean,
    payus_transaction_id: {
        type: String
    },
    payus_amount_orignal: {
        type: Number
    },
    payus_amount_usd: {
        type: Number
    },
    payus_amount_crypto: {
        type: Number
    },
    payus_destination_tag:{
        type: String
    },
    payus_mining_fees:{
        type: Number
    },	
    payus_total_payable_amount:{
        type: Number
    },	
    payus_fees:{		
        type: Number
    },	
    payus_total_fees:{		
        type: Number
    },
    payus_usd_rate:{		
        type: Number
    },
    payus_expire_datetime: {
        type: Date
    },
    payus_payment_tracking:{
        type: String
    },
    payus_status:{
        type: String
    },
    edited_by: { type: Schema.Types.ObjectId, ref: 'Users' },
    edited_at: Date,
    notifyHold: Boolean
}, { timestamps: true });
mongoose.model('Transaction', TransactionSchema);