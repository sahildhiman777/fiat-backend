const mongoose = require('mongoose');

const { Schema } = mongoose;

const BankListSchema = new Schema({
    name: String,
    code: String,
    iban: String,
    country: String,
    status: Boolean,
    created_at: Date,
    updated_at: Date
});
mongoose.model('BankList', BankListSchema);