const mongoose = require('mongoose');

const { Schema } = mongoose;

const BanksSchema = new Schema({
    account_number: String,
    bank_name: String,
    holder_name: String,
    bank_address: String,
    bank_code:String,
    country: String, 
    currency: { type: Schema.Types.ObjectId, ref: 'Currency' },   
    status: Boolean,
    created_at: Date,
    updated_at: Date,
    logo: String,
    type: String
});
mongoose.model('Banks', BanksSchema);