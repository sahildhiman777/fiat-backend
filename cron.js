const cron = require('node-cron');
const request = require('request');
const CryptoJS = require("crypto-js");
const config = require('./config/config');
const mongoose = require('mongoose');
mongoose.promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1:27017/coindashboard');
mongoose.set('debug', true);
require('./models/Transaction');
const Transaction = mongoose.model('Transaction');
cron.schedule('*/1 * * * *', function () {
    cronJob();
});

cronJob = function () {
    console.log('CRON: function called--------');
    
    Transaction.find({
        notifyHold: true,
        status: 'approved',
        deposit: false,
        transaction_hash: {$ne: null}
    }).then(trxs => {
        console.log('CRON: trx fetched--------', trxs);
        
        if(trxs.length){
            trxs.map(t => {                
                let notifyObj = Object.assign({}, t.toObject());
                const updateObj = {};
                updateObj.notifyHold = false;
                updateObj.updated_at = new Date();

                Transaction.findByIdAndUpdate(t._id, updateObj, { new: true }, function (err, model) {
                    if (model) {
                        notifyObj.status = notifyObj.payus_status;
                        notify_exchange(notifyObj);
                        console.log('CRON: trx updated--------', model);
                    } else if (err) {
                        console.log('CRON: trx update error--------', err);
                    }
                });
            })
        }
    }).catch(err => {
        console.log("CRON: Error : ", err);
    });
}

notify_exchange = function (transaction) {
    const notify_url = transaction.notify_url;

    // delete transaction['_id'];
    delete transaction['deposit'];
    delete transaction['edited'];
    delete transaction['updated_at'];
    delete transaction['notify_url'];
    delete transaction['return_url'];
    delete transaction['edited_by'];    
    delete transaction['edited_at'];
    delete transaction['notifyHold'];
    delete transaction['__v'];

    console.log('CRON: Notify transaction', transaction);
    const str = JSON.stringify(transaction);
    const ciphertext = (CryptoJS.AES.encrypt(str, config.secretAes)).toString();
    var options = {
        method: 'POST',
        url: notify_url,
        headers: { 'content-type': 'application/json' },
        body: {
            data: ciphertext
        },
        json: true
    };
    request(options, function (error, response, body) {
        console.log('CRON: Notify response', options, body);
    });
}